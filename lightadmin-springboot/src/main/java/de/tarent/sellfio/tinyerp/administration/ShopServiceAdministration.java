package de.tarent.sellfio.tinyerp.administration;

import org.lightadmin.api.config.AdministrationConfiguration;
import org.lightadmin.api.config.builder.EntityMetadataConfigurationUnitBuilder;
import org.lightadmin.api.config.unit.EntityMetadataConfigurationUnit;
import de.tarent.sellfio.tinyerp.domain.ShopService;

/**
 * Created by mley on 21.01.16.
 */
public class ShopServiceAdministration extends AdministrationConfiguration<ShopService> {
    @Override
    public EntityMetadataConfigurationUnit configuration(EntityMetadataConfigurationUnitBuilder configurationBuilder) {
        return configurationBuilder.nameField("service").singularName("Markt-Eigenschaft").pluralName("Markt-Eigenschaften").build();
    }
}
