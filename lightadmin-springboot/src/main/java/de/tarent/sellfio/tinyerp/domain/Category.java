package de.tarent.sellfio.tinyerp.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by mley on 21.01.16.
 */
@Entity
@Data
public class Category implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String description;
}
