package de.tarent.sellfio.tinyerp.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.List;

/**
 * Created by mley on 21.01.16.
 */
@Entity
@Data
public class Promotion implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name="product")
    private Product product;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Shop> shops;

    @Column
    private Boolean countryWide;

    @Column
    private String description;
}
