package de.tarent.sellfio.tinyerp.administration;

import de.tarent.sellfio.tinyerp.domain.Country;
import org.lightadmin.api.config.AdministrationConfiguration;
import org.lightadmin.api.config.builder.EntityMetadataConfigurationUnitBuilder;
import org.lightadmin.api.config.unit.EntityMetadataConfigurationUnit;

/**
 * Created by mley on 21.01.16.
 */
public class CountryAdministration extends AdministrationConfiguration<Country> {

    @Override
    public EntityMetadataConfigurationUnit configuration(EntityMetadataConfigurationUnitBuilder configurationBuilder) {
        return configurationBuilder.nameField("countryCode").singularName("Land").pluralName("Länder").build();
    }
}
