package de.tarent.sellfio.tinyerp.administration;

import org.lightadmin.api.config.AdministrationConfiguration;
import org.lightadmin.api.config.builder.EntityMetadataConfigurationUnitBuilder;
import org.lightadmin.api.config.unit.EntityMetadataConfigurationUnit;
import de.tarent.sellfio.tinyerp.domain.Promotion;

/**
 * Created by mley on 21.01.16.
 */
public class PromotionAdministration extends AdministrationConfiguration<Promotion> {
    @Override
    public EntityMetadataConfigurationUnit configuration(EntityMetadataConfigurationUnitBuilder configurationBuilder) {
        return configurationBuilder.nameField("description").singularName("Sonderangebot").pluralName("Sonderangebote").build();
    }
}
