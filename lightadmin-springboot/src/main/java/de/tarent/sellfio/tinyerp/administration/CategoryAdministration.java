package de.tarent.sellfio.tinyerp.administration;

import org.lightadmin.api.config.AdministrationConfiguration;
import org.lightadmin.api.config.builder.EntityMetadataConfigurationUnitBuilder;
import org.lightadmin.api.config.unit.EntityMetadataConfigurationUnit;
import de.tarent.sellfio.tinyerp.domain.Category;

/**
 * Created by mley on 21.01.16.
 */
public class CategoryAdministration extends AdministrationConfiguration<Category> {

    @Override
    public EntityMetadataConfigurationUnit configuration(EntityMetadataConfigurationUnitBuilder configurationBuilder) {
        return configurationBuilder.nameField("description").singularName("Kategorie").pluralName("Kategorien").build();
    }
}
