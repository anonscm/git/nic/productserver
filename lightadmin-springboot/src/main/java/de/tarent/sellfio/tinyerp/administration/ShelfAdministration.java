package de.tarent.sellfio.tinyerp.administration;

import de.tarent.sellfio.tinyerp.domain.Shelf;
import org.lightadmin.api.config.AdministrationConfiguration;
import org.lightadmin.api.config.builder.EntityMetadataConfigurationUnitBuilder;
import org.lightadmin.api.config.unit.EntityMetadataConfigurationUnit;

/**
 * Created by mley on 21.01.16.
 */
public class ShelfAdministration extends AdministrationConfiguration<Shelf> {

    @Override
    public EntityMetadataConfigurationUnit configuration(EntityMetadataConfigurationUnitBuilder configurationBuilder) {
        return configurationBuilder.nameField("name").singularName("Regal").pluralName("Regale").build();
    }
}
