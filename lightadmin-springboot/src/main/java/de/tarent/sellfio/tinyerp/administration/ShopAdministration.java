package de.tarent.sellfio.tinyerp.administration;

import de.tarent.sellfio.tinyerp.domain.Shop;
import org.lightadmin.api.config.AdministrationConfiguration;
import org.lightadmin.api.config.builder.EntityMetadataConfigurationUnitBuilder;
import org.lightadmin.api.config.unit.EntityMetadataConfigurationUnit;
import org.lightadmin.api.config.utils.EntityNameExtractor;

/**
 * Created by mley on 21.01.16.
 */
public class ShopAdministration extends AdministrationConfiguration<Shop> {

    @Override
    public EntityMetadataConfigurationUnit configuration(EntityMetadataConfigurationUnitBuilder configurationBuilder) {
        return configurationBuilder.nameExtractor(new EntityNameExtractor<Shop>() {
            @Override
            public String apply(Shop input) {
                return input.getCountry().getCountryCode()+ " " + input.getCity();
            }
        }).singularName("Markt").pluralName("Märkte").build();
    }
}
