package de.tarent.sellfio.tinyerp.administration;

import de.tarent.sellfio.tinyerp.domain.ProductLocation;
import org.lightadmin.api.config.AdministrationConfiguration;
import org.lightadmin.api.config.builder.EntityMetadataConfigurationUnitBuilder;
import org.lightadmin.api.config.unit.EntityMetadataConfigurationUnit;
import org.lightadmin.api.config.utils.EntityNameExtractor;

/**
 * Created by mley on 21.01.16.
 */
public class ProductLocationAdministration extends AdministrationConfiguration<ProductLocation> {

    @Override
    public EntityMetadataConfigurationUnit configuration(EntityMetadataConfigurationUnitBuilder configurationBuilder) {
        return configurationBuilder.nameExtractor(new EntityNameExtractor<ProductLocation>() {
            @Override
            public String apply(ProductLocation input) {
                return input.getProduct().getDescription()+ " "+input.getShelf().getName() + " "+input.getBoard()+ " "+input.getPosition();
            }
        }).singularName("Produktplazierung").pluralName("Produktplazierungen").build();
    }
}
