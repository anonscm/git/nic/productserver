package de.tarent.sellfio.tinyerp.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by mley on 21.01.16.
 */
@Entity
@Data
public class ProductLocation implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name="product")
    private Product product;

    @ManyToOne
    @JoinColumn(name="shelf")
    private Shelf shelf;

    @Column
    private Integer board;

    @Column
    private Double position;
}
