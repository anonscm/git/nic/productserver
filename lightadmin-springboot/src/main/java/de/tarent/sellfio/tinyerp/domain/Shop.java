package de.tarent.sellfio.tinyerp.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.List;

/**
 * Created by mley on 21.01.16.
 */
@Entity
@Data
public class Shop implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "country")
    private Country country;

    @Column
    private String city;

    @ManyToMany
    List<ShopService> services;
}
