package de.tarent.sellfio.tinyerp.domain;

import lombok.Data;
import org.lightadmin.api.config.annotation.FileReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by mley on 21.01.16.
 */
@Entity
@Data
public class Product implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="category")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "country")
    private Country country;

    @Column
    private String description;

    @Column
    private Integer price;

    @FileReference(baseDirectory = "/tmp/")
    private String image;
}
