
insert into category(description) values ('Molkereiprodukte')
insert into category(description) values ('Getränke')

insert into country(country_code) values ('DE')

insert into shop_service(service) values ('Parkplatz')
insert into shop_service(service) values ('Brauerei')

insert into shop(country, city) values (1, 'Bonn')

insert into shop_services (shop_id, services_id) values (1, 1)
insert into shop_services (shop_id, services_id) values (1, 2)

insert into product(category, country, description, price) values(1, 1, 'Milch', 109)
insert into product(category, country, description, price) values(2, 1, 'Bönnsch', 159)

insert into promotion (country_wide, description, product) values (false, 'Bönnsch im Angebot', 2)

insert into shelf(name, shop, num_of_boards) values ('Obergärige Biere', 1, 1);

insert into product_location(product, shelf, board, position) values (2, 1, 0, 0);

