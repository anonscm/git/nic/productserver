package de.tarent.sellfio.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple expiring cache implementation. Cache is only refreshed when values are requested, but not when cached items
 * are about to expire.
 * Created by mley on 20.04.15.
 */
public class SimpleCache<K, V> {

    private long period;
    private CacheResolver<K, V> resolver;

    private Map<K, Entry<V>> cacheMap;

    /**
     * Creates a new SimpleCache
     *
     * @param resolver the resolver
     * @param period   caching period in milliseconds. If a value in the cache is older than period it is requested
     *                 again.
     */
    public SimpleCache(CacheResolver<K, V> resolver, long period) {
        this.period = period;
        this.resolver = resolver;

        cacheMap = new HashMap<>();
    }

    /**
     * Gets a value from the cache. If the value is not cached or the cached value is too old, the resolver is used
     * to get a fresh value.
     *
     * @param key the key
     * @return the value.
     */
    public V get(K key) {
        Entry<V> entry = cacheMap.get(key);
        long now = System.currentTimeMillis();
        if (entry == null) {
            entry = new Entry<>();
            entry.storedOn = now - period - 1;
        }

        if (entry.storedOn + period < now) {
            entry.value = resolver.resolve(key);
            entry.storedOn = now;
            cacheMap.put(key, entry);
        }

        return entry.value;
    }

    /**
     * Entry object for the cache
     *
     * @param <V>
     */
    private static class Entry<V> {
        /**
         * the value
         */
        V value;

        /**
         * timestamp when the value was stored
         */
        long storedOn;
    }
}
