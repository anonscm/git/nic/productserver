package de.tarent.sellfio.util;

/**
 * Created by mley on 20.04.15.
 */
public interface CacheResolver<K, V> {

    /**
     * Resolve a value.
     * @param key the key
     * @return the value.
     */
    V resolve(K key);
}
