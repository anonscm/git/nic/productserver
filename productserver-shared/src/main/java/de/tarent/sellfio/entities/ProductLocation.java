package de.tarent.sellfio.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by mley on 02.10.14.
 */
@Entity
@Data
@Table(name = "productlocation")
@JsonIgnoreProperties({"id", "articleId"})
public class ProductLocation {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "_id")
    private long id;

    /**
     * The article ID.
     */
    @Column(name = "pl_articleid")
    private int articleId;

    /**
     * Name of the shelf, the product is located in.
     */
    private String shelf;

    /**
     * horizontal position of product on the shelf board.
     */
    private int x;
    /**
     * number of the shelf board where product is located.
     */
    private int y;

    public ProductLocation(String shelf, int x, int y) {
        this.shelf = shelf;
        this.x = x;
        this.y = y;
    }

    public ProductLocation() {
    }

}
