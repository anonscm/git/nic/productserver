package de.tarent.sellfio.entities;

import lombok.Data;

/**
 * Created by mley on 29.10.14.
 */
@Data
public class CheckoutResponse {

    private String eanCode;
    private int status;
    private String message;

    public static CheckoutResponse respond(int status, String message) {
        CheckoutResponse r  =new CheckoutResponse();
        r.setStatus(status);
        r.setMessage(message);
        return r;
    }

    public static CheckoutResponse respond(String ean) {
        CheckoutResponse response = new CheckoutResponse();
        response.setStatus(0);
        response.setMessage("SUCCESS");
        response.setEanCode(ean);
        return response;
    }
}
