package de.tarent.sellfio.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by mley on 29.10.14.
 */
@Data
@Entity
public class ConfigItem {

    @Id
    private String key;
    private String value;
}
