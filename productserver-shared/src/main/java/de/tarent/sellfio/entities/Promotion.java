package de.tarent.sellfio.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by mley on 01.10.14.
 */
@Entity
@NamedQueries(value = {
        @NamedQuery(name = Promotion.BY_SHOP, query = "SELECT p FROM Promotion p " +
                "where p.shop = :shop and p.validFrom <= :nowDate and p.validTo >= :nowDate" )
})
@Data
public class Promotion {


    public static final String BY_SHOP = "Promotion.byShop";
    @Id
    @GeneratedValue
    private long id;

    /**
     * shop which the promotion is for
     */
    private long shop;

    private long promotionId;

    private Date validFrom;

    private Date validTo;

    /**
     * Type of promotion
     */
    private String typeId;

    /**
     * Promotion requires a customer fidelity card.
     */
    private boolean fidelityCardRequired;

    /**
     * Promotion is bound to an article with this is id if value is >0.
     */
    private int articleId = -1;


    private String param1;
    private String param2;
    private String param3;

    /**
     * Employee card required to enable this promotion
     */
    private boolean employeesOnly;

    /**
     * Limits the occurence of the promotion per bon.
     */
    private int promotionLimit;

    /**
     * if > 0, this is the promotion of the week. The higher the value, the more important is the promotion.
     */
    private int highlight;

    /**
     * Default constructor
     */
    public Promotion() {
    }

    /**
     * Constructor.
     *
     * @param line the promotion line
     * @throws java.text.ParseException if something went wrong during parsing
     */
    public Promotion(final String line) throws ParseException {
        // 952230229;999993;04-SET-14;31-OTT-14;99999;SCONTO ARTICOLO €;0;790503;;;1;10;0;; NOSONAR
        final String[] values = line.split(";", -1);
        final DateFormat dateParser = new SimpleDateFormat("dd-MMM-yy", Locale.ITALIAN);
        promotionId = Integer.parseInt(values[1]);
        validFrom = dateParser.parse(values[2]);
        validTo = dateParser.parse(values[3]);
        switch (values[4]) {
            case "84e6e5d0-ac1e-11de-8a39-0800200c9a66":
                typeId = "PERCENT_WEIGHABLE";
                break;
            case "94e6e5d0-ac1e-11de-8a39-0800200c9a66":
                typeId = "PERCENT";
                break;
            case "99999":
                typeId = "ABSOLUTE";
                break;
            case "ccb4cc62-b65d-4117-82d6-e264d2d7cc2d":
                typeId = "SPECIAL";
                break;

        }

        fidelityCardRequired = "1".equals(values[6]);
        articleId = values[7].length() > 0 ? Integer.parseInt(values[7]) : -1;
        param1 = values[10];
        param2 = values[11];
        param3 = values[12];
        employeesOnly = "1".equals(values[13]);
        promotionLimit = values[14].length() > 0 ? Integer.parseInt(values[14]) : -1;
    }

}
