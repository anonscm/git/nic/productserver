package de.tarent.sellfio.entities;

import lombok.Data;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderColumn;
import java.util.List;

/**
 * Created by mley on 07.10.14.
 */
@Entity
@NamedQueries(value = {
        @NamedQuery(name = Shelf.ALL, query = "SELECT s FROM Shelf s ")
})
@Data
public class Shelf {

    public static final String ALL = "Shelf.all";

    @Id
    String shelfName;

    @OrderColumn(name="board_order")
    @ElementCollection(fetch = FetchType.EAGER, targetClass = Integer.class)
    List<Integer> maxXs;
}
