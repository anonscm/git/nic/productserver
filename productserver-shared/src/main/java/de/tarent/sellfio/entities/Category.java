package de.tarent.sellfio.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Created by mley on 18.09.14.
 */
@Entity
@NamedQueries(value = {
        @NamedQuery(name = Category.BY_DESC, query = "SELECT c FROM Category c " +
                "where UPPER(c.description) like UPPER(:search)")
})
@Data
public class Category {

    public static final String BY_DESC = "Category.byDesc";

    /**
     * Category id
     */
    @Id
    private String categoryId;

    /**
     * German description
     */
    private String description;


    /**
     * Number of products in this category
     */
    private int numberOfProducts;

}
