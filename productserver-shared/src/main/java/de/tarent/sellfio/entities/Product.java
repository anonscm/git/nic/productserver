package de.tarent.sellfio.entities;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by mley on 18.09.14.
 */
@Entity
@NamedQueries(value = {
        @NamedQuery(name = Product.ALL, query = "SELECT p FROM Product p"),
        @NamedQuery(name = Product.BY_EAN, query = "SELECT p FROM Product p where :ean member of p.eans"),
        @NamedQuery(name = Product.BY_DESC, query = "SELECT p FROM Product p " +
                "where UPPER(p.description) like UPPER(:search) or UPPER(p.shortDescription) like UPPER(:search) "),
        @NamedQuery(name = Product.BY_CATEGORY, query = "SELECT p FROM Product p where p.categoryId like :category")
})
@Table(name = "product", indexes = { @Index(name = "categoryIndex", columnList = "categoryId"),
        @Index(name = "descriptionIndex", columnList = "description") })
@Data
public class Product {

    public static final String ALL = "Product.all";
    public static final String BY_EAN = "Product.byEAN";
    public static final String BY_DESC = "Product.byDesc";
    public static final String BY_CATEGORY = "Product.byCategory";
    /**
     * The internal article ID of the shop
     */
    @Id
    @Column(name = "_id")
    private int articleId;
    /**
     * Short description / product name
     */
    private String shortDescription;
    /**
     * Description of the product
     */
    private String description;

    /**
     * List of EANs of the product. One product can have multiple EANs
     */
    @ElementCollection(fetch = FetchType.EAGER, targetClass = String.class)
    @CollectionTable(name="product_eans", joinColumns = @JoinColumn(name="_id"))
    private Set<String> eans;

    /**
     * product price in eurocents
     */
    private int price;

    /**
     * merceological department, required for promotions
     */
    private int mercId;

    /**
     * Category id of the product
     */
    private String categoryId;

    /**
     * unit of measure of this product. Details TBD. This can be
     * BY_PIECE or e.g. BY_KILOGRAMM or BY_GRAMM
     */
    private String unitMeasure;

    /**
     * flag if product is weighable
     */
    private boolean weighable;

    /**
     * flag if product price is encoded in EAN
     */
    private boolean embeddedPrice;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "pl_articleid", referencedColumnName="_id", nullable = true)
    private Set<ProductLocation> locations = new HashSet<>();

    @Transient
    private String image;

    public Product() {

    }

    public Product(String line) {
        final String[] values = line.split(Pattern.quote("|"), -1);
        articleId = Integer.parseInt(values[0]);
        description = values[1];
        eans = new HashSet(Arrays.asList(values[2].split(";")));
        price = Integer.parseInt(values[3].replaceAll("\\.", ""));
        unitMeasure = values[4];
        mercId = Integer.parseInt(values[5]);
        weighable = "1".equals(values[6]);
        categoryId = values[7];
    }

}
