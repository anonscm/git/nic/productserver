package de.tarent.sellfio.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mley on 23.10.14.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cart {

    private List<Article> articles = new ArrayList<>();

    private List<String> additionalBarcodes = new ArrayList<>();

}
