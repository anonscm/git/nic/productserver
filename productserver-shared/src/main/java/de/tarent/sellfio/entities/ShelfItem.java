package de.tarent.sellfio.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by mley on 18.09.14.
 * Entity for ShelfItem. A ShelfItem is an area in a board in a shelf where one article is stored.
 */
@Entity
@NamedQueries(value = {
        @NamedQuery(name = ShelfItem.BY_ARTICLE, query = "SELECT i FROM ShelfItem i where i.articleId = :articleId"),
        @NamedQuery(name = ShelfItem.BY_GROUP, query = "SELECT i FROM ShelfItem i where i.categoryId = :categoryId"),
        @NamedQuery(name = ShelfItem.BY_GROUP_RANGE, query = "SELECT i FROM ShelfItem i " +
                "where i.categoryId like :categoryId")
})
@Table(indexes = {
        @Index(columnList = "shelfName"),
        @Index(columnList = "articleId"),
        @Index(columnList = "shelfName"),
        @Index(columnList = "categoryId")})
@Data
public class ShelfItem {

    public static final String BY_ARTICLE = "ShelfItem.byArticle";
    public static final String BY_GROUP = "ShelfItem.byGroup";
    public static final String BY_GROUP_RANGE = "ShelfItem.byGroupRange";
    @Id
    @GeneratedValue
    private int shelfItemId;

    /**
     * Shelfname as from space man
     */
    private String shelfName;

    /**
     * horizontal position of the product on the board
     */
    private int x;

    /**
     * vertical position of the shelf board
     */
    private int y;

    /**
     * article id
     */
    private int articleId;

    /**
     * article group id
     */
    private String categoryId;

    private int maxX;
    private int maxY;

}
