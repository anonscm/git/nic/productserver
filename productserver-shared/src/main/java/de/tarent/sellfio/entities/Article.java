package de.tarent.sellfio.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Created by mley on 23.10.14.
 */
@Data
@JsonIgnoreProperties(value = {"product"}, ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Article {

    private String ean;
    private int quantity;
    private int weight;
    private int price;
    private boolean promotionPrice;
    private Product product;
    private Promotion promotion;
}
