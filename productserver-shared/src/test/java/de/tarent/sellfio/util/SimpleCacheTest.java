package de.tarent.sellfio.util;

import junit.framework.TestCase;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by mley on 20.04.15.
 */
public class SimpleCacheTest {

    boolean resolverHit = false;

    @Test
    public void testCache() throws InterruptedException {
        CacheResolver<String, String> resolver = new CacheResolver<String, String>() {
            @Override
            public String resolve(String key) {
                resolverHit = true;
                return "value"+key;

            }
        };

        SimpleCache<String, String> cache = new SimpleCache<>(resolver, 10);

        String v1 = cache.get("key");
        assertTrue(resolverHit);
        assertNotNull(v1);

        resolverHit = false;

        String v2 = cache.get("key");
        assertFalse(resolverHit);
        TestCase.assertNotNull(v2);
        assertTrue(v1 == v2);

        Thread.sleep(11);

        resolverHit = false;

        String v3 = cache.get("key");
        assertTrue(resolverHit);
        assertNotNull(v3);
        assertFalse(v3 == v1);

    }
}
