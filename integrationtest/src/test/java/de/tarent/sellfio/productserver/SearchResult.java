package de.tarent.sellfio.productserver;

import de.tarent.sellfio.entities.Category;
import de.tarent.sellfio.entities.Product;
import lombok.Data;

import java.util.List;

/**
 * Created by mley on 25.02.15.
 */
@Data
public class SearchResult {

    private List<Product> products;
    private List<Category> categories;
}
