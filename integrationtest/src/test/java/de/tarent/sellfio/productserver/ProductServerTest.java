package de.tarent.sellfio.productserver;

import de.tarent.sellfio.entities.Product;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.fail;

/**
 * Created by mley on 25.02.15.
 */

public class ProductServerTest {



    private static String endPoint = "http://invio-mapserver.lan.tarent.de:8080/productserver/";

    public static Product getProduct(RestTemplate rest, int id) {
        return rest.getForObject(endPoint+"product/"+id, Product.class);
    }

    public static SearchResult search(RestTemplate rest, String s) {
         return rest.getForObject(endPoint + "search/?s="+s, SearchResult.class);
    }

    @Test
    public void doLoadTest() {
        loadTestSearch(1);
        loadTestSearch(5);
        loadTestSearch(10);
        loadTestSearch(20);
        loadTestSearch(100);
    }


    public void loadTestSearch(int numOfThreads) {
        RestTemplate rest = new RestTemplate();

        final List<Long> responseTimes = Collections.synchronizedList(new ArrayList<Long>(1000));

        SearchResult result = search(rest, "a");
        final List<String> searchTerms = new ArrayList<>();
        for(Product p : result.getProducts()) {
            searchTerms.add(p.getDescription().split(" ")[0]);
        }

        System.out.println("["+numOfThreads+" threads] Search terms: "+searchTerms.size());
        long duration = System.currentTimeMillis();
        List<Thread> threads = new ArrayList<>();
        for(int i=0 ;i<numOfThreads; i++) {
            Thread t = new Thread("Test"+i){
                RestTemplate r = new RestTemplate();
                public void run() {
                    try {
                        for (String s : searchTerms) {
                            long l = System.currentTimeMillis();
                            search(r, s);
                            responseTimes.add(System.currentTimeMillis() - l);
                        }
                    } catch(Exception e) {
                        e.printStackTrace();
                        fail("FAIL"+e.getMessage());
                    }
                }
            };
            t.start();
            threads.add(t);
        }

        for(Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                fail("Interrupted");
            }
        }

        duration = System.currentTimeMillis() - duration;

        Collections.sort(responseTimes);
        long sum = 0l;
        for(Long l : responseTimes) {
            sum += l;
        }

        System.out.println("[" + numOfThreads + " threads] Requests: " + responseTimes.size());
        System.out.println("["+numOfThreads+" threads] Duration (s): "+duration/1000.0);
        System.out.println("["+numOfThreads+" threads] Request time (s): "+sum/1000.0);
        System.out.println("["+numOfThreads+" threads] avg. requests/s: "+(responseTimes.size()/(duration/1000.0)));
        System.out.println("["+numOfThreads+" threads] shortest (ms): "+responseTimes.get(0));
        System.out.println("["+numOfThreads+" threads] longest (ms): "+responseTimes.get(responseTimes.size()-1));
        System.out.println("["+numOfThreads+" threads] median (ms): "+responseTimes.get(responseTimes.size()/2));
        System.out.println("["+numOfThreads+" threads] average: "+(sum/responseTimes.size()));
        System.out.println();
    }
}
