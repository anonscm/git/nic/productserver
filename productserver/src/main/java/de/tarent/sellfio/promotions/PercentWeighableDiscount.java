package de.tarent.sellfio.promotions;

import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Promotion;

/**
 * An implementation for the percentage {@link Discount} on weighable products.
 */
public class PercentWeighableDiscount implements Discount {

    @Override
    public void applyDiscount(final Promotion promotion, final Article article,
                              final PromotionManager promotionManager) {

        final double weightKg = article.getWeight() / 1000.0;

        if (weightKg < Double.parseDouble(promotion.getParam1().replaceAll(",", "."))) {
            return;
        }

        final double discountFactor = 1 - (Double.parseDouble(promotion.getParam2().replaceAll(",", ".")) / 100.0);
        final int undiscountedPrice = article.getPrice();
        final int discountedPrice = (int) (undiscountedPrice * discountFactor);
        article.setPrice(discountedPrice);
        article.setPromotionPrice(true);
    }

    @Override
    public String getType() {
        return PromotionManager.PERCENT_WEIGHABLE;
    }


}
