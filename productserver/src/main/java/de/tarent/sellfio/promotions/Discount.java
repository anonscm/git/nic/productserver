package de.tarent.sellfio.promotions;

import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Promotion;

/**
 * An interface for the penny discounts.
 */
public interface Discount {

    /**
     * Apply a discount to the article.
     *
     * @param promotion the {@link Promotion}
     * @param article the {@link Article} receiving the discount
     * @param promotionManager the PromotionManager
     */
    void applyDiscount(final Promotion promotion, final Article article, final PromotionManager promotionManager);

    /**
     * Gets the type.
     *
     * @return the type as a string
     */
    String getType();
}
