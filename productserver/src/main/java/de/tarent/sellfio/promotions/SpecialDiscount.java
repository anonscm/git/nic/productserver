package de.tarent.sellfio.promotions;

/**
 * Created by mley on 07.11.14.
 */

import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Promotion;

/**
 * An implementation of the special {@link Discount} on products. (price is set to something else)
 */
public class SpecialDiscount implements Discount {

    @Override
    public void applyDiscount(final Promotion promotion, final Article article,
                              final PromotionManager promotionManager) {
        if (promotionManager.promotionLimitReached(promotion)) {
            return;
        }

        final int newPrice = Integer.parseInt(promotion.getParam2().replaceAll(",", "").replaceAll("\\.", ""));

        article.setPrice(Math.max(newPrice, 0));
        article.setPromotionPrice(true);

        promotionManager.updateIssuedDiscountCount(promotion, 1);
    }

    @Override
    public String getType() {
        return PromotionManager.SPECIAL;
    }
}
