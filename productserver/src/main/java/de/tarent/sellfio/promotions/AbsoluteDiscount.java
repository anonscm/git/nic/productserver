package de.tarent.sellfio.promotions;


import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Promotion;


/**
 * An implementation of the absolute {@link Discount} on products.
 */
public class AbsoluteDiscount implements Discount {
    @Override
    public void applyDiscount(final Promotion promotion, final Article article,
                              final PromotionManager promotionManager) {

        if (promotionManager.promotionLimitReached(promotion)) {
            return;
        }

        int unitDiscount = Integer.parseInt(promotion.getParam2().replaceAll(",", "").replaceAll("\\.", ""));
        if (!promotion.getParam2().contains(",") && !promotion.getParam2().contains(".")) {
            unitDiscount *= 100;
        }

        article.setPrice(Math.max(article.getPrice() - unitDiscount, 0));
        article.setPromotionPrice(true);

        promotionManager.updateIssuedDiscountCount(promotion, 1);
    }

    @Override
    public String getType() {
        return PromotionManager.ABSOLUTE;
    }
}
