package de.tarent.sellfio.promotions;


import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Promotion;

/**
 * An implementation for the percentage discounts in the penny application.
 */
public class PercentDiscount implements Discount {

    @Override
    public void applyDiscount(final Promotion promotion, final Article article,
                              final PromotionManager promotionManager) {


        if (promotionManager.promotionLimitReached(promotion)) {
            return;
        }

        final double discountFactor = 1 - (Double.parseDouble(promotion.getParam2().replaceAll(",", ".")) / 100.0);
        final int unitPrice = article.getPrice();
        final int discountedPrice = (int) (unitPrice * discountFactor);
        article.setPrice(discountedPrice);
        article.setPromotionPrice(true);

        promotionManager.updateIssuedDiscountCount(promotion, 1);
    }


    @Override
    public String getType() {
        return PromotionManager.PERCENT;
    }


}
