package de.tarent.sellfio.promotions;


import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Cart;
import de.tarent.sellfio.entities.Promotion;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manages the {@link Promotion}s for the Penny application.
 */
public class PromotionManager {

    /**
     * Code for the percentage discount on weighable products.
     */
    public static final String PERCENT_WEIGHABLE = "PERCENT_WEIGHABLE";

    /**
     * Code for the percentage discount of non-weighable/normal products.
     */
    public static final String PERCENT = "PERCENT";

    /**
     * Code for the absolute DISCOUNTS on products.
     */
    public static final String ABSOLUTE = "ABSOLUTE";

    /**
     * Code for the special discount price on products.
     */
    public static final String SPECIAL = "SPECIAL";


    private static final Logger LOG = Logger.getLogger(PromotionManager.class);

    private static final Map<String, Discount> DISCOUNTS;

    static {
        final Map<String, Discount> discounts = new HashMap<>();
        addDiscount(discounts, new AbsoluteDiscount());
        addDiscount(discounts, new PercentDiscount());
        addDiscount(discounts, new PercentWeighableDiscount());
        addDiscount(discounts, new SpecialDiscount());
        DISCOUNTS = Collections.unmodifiableMap(discounts);
    }

    private Map<Long, Integer> issuedPromotionsCount = new HashMap<>();

    private List<Promotion> promotions;

    /**
     * Creates a new PromotionManager.
     *
     * @param promotions List of List of Promotion objects
     */
    public PromotionManager(List<Promotion> promotions) {
        this.promotions = promotions;

    }

    /**
     * Applies the valid promotions to the shoppingCart. The contents of the shopping cart must be normalized, that
     * all Articles and Products are unique and not more than once in the cart. The amount of products must therefore be
     * represented in the Article#quantity.
     *
     * @param shoppingCart ShoppingCart
     */
    public void applyPromotion(final Cart shoppingCart) {
        issuedPromotionsCount.clear();

        final List<Article> articles = explodeArticles(shoppingCart.getArticles());

        for (Article a : articles) {
            applyPromotions(a, shoppingCart);
        }

        shoppingCart.getArticles().clear();
        shoppingCart.getArticles().addAll(compactArticles(articles));
    }


    /**
     * Set the list of promotions
     *
     * @param promotions List of Promotion objects
     */
    public void setPromotions(List<Promotion> promotions) {
        this.promotions = promotions;
    }

    /**
     * Updates the issued discount quantity.
     *
     * @param promotion   the {@link Promotion}
     * @param issuedCount the newly issued quantity
     */
    public void updateIssuedDiscountCount(final Promotion promotion, final int issuedCount) {

        int alreadyIssued = 0;

        if (issuedPromotionsCount.containsKey(promotion.getPromotionId())) {
            alreadyIssued = issuedPromotionsCount.get(promotion.getPromotionId());
        }
        alreadyIssued += issuedCount;

        issuedPromotionsCount.put(promotion.getPromotionId(), alreadyIssued);
    }

    /**
     * Gets the number of already issued promotions
     *
     * @param promotion Promotion object
     * @return the quantity how many this promotion has been issued in the current shopping cart
     */
    public int getIssuedPromotionCount(final Promotion promotion) {
        int issuedCount = 0;

        if (issuedPromotionsCount.containsKey(promotion.getPromotionId())) {
            issuedCount = issuedPromotionsCount.get(promotion.getPromotionId());
        }

        return issuedCount;
    }


    /**
     * Checks whether or not the limit of a discount has been reached.
     *
     * @param promotion the {@link Promotion}
     * @return true if the limit is reached, false if not
     */
    public boolean promotionLimitReached(final Promotion promotion) {
        if (promotion.getPromotionLimit() > 0) {
            final int issuedDiscounts = getIssuedPromotionCount(promotion);
            return issuedDiscounts >= promotion.getPromotionLimit();
        }
        return false;
    }

    /**
     * Compact the list of articles. Same products with same price will be aggregated.
     *
     * @param articles List of articles
     * @return compacted list of articles
     */
    private List<Article> compactArticles(final List<Article> articles) {
        final List<Article> compacted = new ArrayList<>();

        Article lastArticle = null;
        for (Article a : articles) {
            if (canBeCompacted(lastArticle, a)) {
                lastArticle.setQuantity(lastArticle.getQuantity() + a.getQuantity());
            } else {
                lastArticle = a;
                compacted.add(lastArticle);
            }
        }

        for (Article a : compacted) {
            a.setPrice(a.getPrice() * a.getQuantity());
        }

        return compacted;
    }

    /**
     * Tests if two articles can be compacted.
     *
     * @param lastArticle the last article
     * @param a           the new
     * @return true is articles can be compacted.
     */
    private boolean canBeCompacted(final Article lastArticle, final Article a) {
        if (lastArticle == null) {
            return false;
        }

        if (lastArticle.getProduct().getArticleId() != a.getProduct().getArticleId()) {
            return false;
        }

        if (lastArticle.getProduct().isEmbeddedPrice() || a.getProduct().isEmbeddedPrice()) {
            return false;
        }

        if (lastArticle.getProduct().isWeighable() || a.getProduct().isWeighable()) {
            return false;
        }

        if (lastArticle.isPromotionPrice() != a.isPromotionPrice()) {
            return false;
        }

        if (lastArticle.getPrice() != a.getPrice()) {
            return false;
        }


        return true;
    }

    /**
     * Explodes the article list. All articles where quantity is > 1 are added multiple times to the result list.
     *
     * @param articles input list
     * @return exploded article list
     */
    private List<Article> explodeArticles(final List<Article> articles) {
        final List<Article> exploded = new ArrayList<>();

        for (Article a : articles) {
            if (a.getProduct().isWeighable() || a.getProduct().isEmbeddedPrice()) {
                a.setQuantity(1);
            }

            for (int i = 0; i < a.getQuantity(); i++) {
                final Article newArticle = createNewArticle(a);
                exploded.add(newArticle);
            }
        }

        return exploded;

    }

    /**
     * Create a new article with quantity one from an existing article
     * @param a existing article
     * @return new article object
     */
    private Article createNewArticle(Article a) {
        final Article newArticle = new Article();
        newArticle.setQuantity(1);
        newArticle.setProduct(a.getProduct());
        newArticle.setWeight(a.getWeight());
        newArticle.setEan(a.getEan());
        if (a.getProduct().isWeighable()) {
            final double preRound = (a.getProduct().getPrice() / 100.0) * (a.getWeight() / 1000.0);
            final Double doublePrice = (BigDecimal.valueOf(preRound)
                    .setScale(2, BigDecimal.ROUND_UP).doubleValue()) * 100;
            newArticle.setPrice(doublePrice.intValue());
        } else {
            newArticle.setPrice(a.getProduct().getPrice());
        }
        newArticle.setPromotionPrice(false);
        newArticle.setPromotion(a.getPromotion());

        return newArticle;
    }

    /**
     * Apply promotions to one article.
     *
     * @param article      single article (quantity must be 1)
     * @param shoppingCart the cart
     */
    private void applyPromotions(final Article article, final Cart shoppingCart) {
        final List<Promotion> validPromotions = getValidPromotions(article, shoppingCart);

        for (final Promotion p : validPromotions) {
            // yes, all promotions for an article will be applied.
            final Discount discount = DISCOUNTS.get(p.getTypeId());
            if (discount != null) {
                discount.applyDiscount(p, article, this);
            }
        }
    }

    /**
     * Returns a list of valid promotions for the article.
     *
     * @param article      article
     * @param shoppingCart cart
     * @return List of Promotion objects
     */
    private List<Promotion> getValidPromotions(final Article article, final Cart shoppingCart) {
        final List<Promotion> validPromotions = new ArrayList<>();

        for (final Promotion promotion : promotions) {
            if (promotionIsValid(promotion, article, shoppingCart)) {
                validPromotions.add(promotion);
            }
        }

        return validPromotions;

    }

    /**
     * Tests if a promotions is valid for an article.
     *
     * @param promotion    Promotion
     * @param article      Article
     * @param shoppingCart cart
     * @return true if all required conditions match
     */
    private boolean promotionIsValid(final Promotion promotion, final Article article,
                                     final Cart shoppingCart) {

        // check promotion date
        final Date currentTime = new Date();
        if (currentTime.before(promotion.getValidFrom()) || currentTime.after(promotion.getValidTo())) {
            return false;
        }

        // check employees only
        if (promotion.isEmployeesOnly() && !hasEmployeeCode(shoppingCart.getAdditionalBarcodes())) {
            return false;
        }

        // check for Fidelity/Loyalty-Card
        if (promotion.isFidelityCardRequired() && !hasFidelityCard(shoppingCart.getAdditionalBarcodes())) {
            return false;
        }

        // check if promotion is bound to an article and if article id matches
        if (promotion.getArticleId() > 0 && promotion.getArticleId() != article.getProduct().getArticleId()) {
            return false;
        }

        return true;
    }

    private boolean hasFidelityCard(final List<String> additionalBarcodes) {
        return checkBarcodes(additionalBarcodes, 2095010000000L, 2095019999999L); //FIXME still pennycard values
    }

    private boolean hasEmployeeCode(final List<String> additionalBarcodes) {
        return checkBarcodes(additionalBarcodes, 2090010000000L, 2090019999999L); //FIXME still penny employee card nrs.
    }

    private boolean checkBarcodes(final List<String> additionalBarcodes, final long min, final long max) {
        for (final String barcode : additionalBarcodes) {
            try {
                final long code = Long.parseLong(barcode);
                if (code >= min && code <= max) {
                    return true;
                }
            } catch (final NumberFormatException e) {
                LOG.error("error parsing number", e);
            }
        }
        return false;
    }


    private static void addDiscount(final Map<String, Discount> discounts, final Discount discount) {
        discounts.put(discount.getType(), discount);
    }

}
