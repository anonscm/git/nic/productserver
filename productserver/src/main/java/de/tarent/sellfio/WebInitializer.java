package de.tarent.sellfio;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.EnumSet;

/**
 * Created by mley on 12.11.14.
 */
public class WebInitializer implements WebApplicationInitializer {

    /**
     * Configure application.
     * @param servletContext ServletContext object.
     * @throws javax.servlet.ServletException if initialization fails
     */
    @Override
    public void onStartup(ServletContext servletContext)
            throws ServletException {

        final AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
        appContext.register(Config.class);
        appContext.setDisplayName("product server");

        final AnnotationConfigWebApplicationContext mvcContext = new AnnotationConfigWebApplicationContext();
        mvcContext.register(WebConfig.class);

        final ServletRegistration.Dynamic springmvc = servletContext.addServlet("productserver",
                new DispatcherServlet(mvcContext));
        springmvc.setAsyncSupported(true);
        springmvc.setLoadOnStartup(1);
        springmvc.addMapping("/");

        final EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD);

        final CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);

        final FilterRegistration.Dynamic characterEncoding = servletContext.addFilter("characterEncoding",
                characterEncodingFilter);
        characterEncoding.addMappingForUrlPatterns(dispatcherTypes, true, "/*");

        servletContext.addListener(new ContextLoaderListener(appContext));
    }

}
