package de.tarent.sellfio.exception;

import de.tarent.sellfio.entities.CheckoutResponse;
import lombok.Data;

/**
 * Created by mley on 10.11.14.
 */
@Data
public class CheckoutException extends Exception {

    private int status;
    private String message;

    public CheckoutException(int status, String message) {
        super();
        this.status = status;
        this.message = message;
    }

    public CheckoutResponse toResponse() {
        return CheckoutResponse.respond(status, message);
    }
}
