package de.tarent.sellfio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * This exception, and corresponding http status 404, is appropriate when an invalid/unknown product was requested via
 * URI path parameter.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ProductNotFoundException extends RuntimeException  {

    public ProductNotFoundException(String message) {
        super(message);
    }

}