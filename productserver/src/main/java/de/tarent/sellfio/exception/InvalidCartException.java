package de.tarent.sellfio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * This exception, and corresponding http status 400, is appropriate when an invalid shopping cart was uploaded by the
 * client in a POST request.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidCartException extends RuntimeException  {

    public InvalidCartException(String message) {
        super(message);
    }

}