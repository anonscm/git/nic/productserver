package de.tarent.sellfio.csvimport;

import de.tarent.sellfio.entities.Product;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 * This class is used for bulk import of data.
 *
 * TODO: the running-in-parallel-protection from the ImportController might be better put directly in this class,
 *       because it is also triggered by a timer, without going through the ImportController.
 */
@EnableAsync
@EnableScheduling
@Transactional
@Component
public class PennyCSVImporter {

    private static final Logger LOG = Logger.getLogger(PennyCSVImporter.class);

    private static final String SEPARATOR = "|";

    @Value("${import.csvdir}")
    String csvDirectoryPath;


    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private CategoryImport categoryImport;
    @Autowired
    private ProductImport productImport;
    @Autowired
    private ShelfItemImport shelfItemImport;
    @Autowired
    private PromotionImport promotionImport;

    private File productFile;
    private File promotionsFile;
    private File categoryFile;
    private File spacemanFile;
    private File tarentSpacemanFile;


    /**
     * Execute this function to begin the import of all csv files into the database.
     */
    @Transactional(readOnly = false)
    @Scheduled(cron="${import.cron}")
    public void run() {
        prepareFiles();

        cleanTables();

        LOG.info("Start of Import. Reading files...");

        importProductsCategoryShelves();

        LOG.info("Products, Categories and Shelves imported. Flushing and clearing...");

        // after hibernate assisted import is done, we flush all pending actions
        sessionFactory.getCurrentSession().flush();
        // and clear the entity cache
        sessionFactory.getCurrentSession().clear();

        LOG.info("Importing promotions...");

        // now we import the promotions directly in SQL, because the data is too much for a small server to
        // import with hibernate
        importPromotions();

        LOG.info("All imported.");
    }

    /**
     * Imports the promotions
     */
    private void importPromotions() {
        sessionFactory.getCurrentSession().doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                promotionImport.setConnection(connection);
                promotionImport.doImport(SEPARATOR, promotionsFile);
                promotionImport.finalizeImport();
            }
        });
    }

    /**
     * Imports product, category and shelf data
     */
    private void importProductsCategoryShelves() {
        productImport.doImport(SEPARATOR, productFile);

        categoryImport.setProductCount(productImport.getProductCount());
        categoryImport.doImport(SEPARATOR, categoryFile);

        final Map<Integer, Product> products = productImport.getProducts();

        shelfItemImport.setProducts(products);
        shelfItemImport.doImport("|", spacemanFile);

        if (tarentSpacemanFile != null && tarentSpacemanFile.exists()) {
            shelfItemImport.doImport("|", tarentSpacemanFile);
        }


        productImport.finalizeImport();
        categoryImport.finalizeImport();
        shelfItemImport.finalizeImport();
    }

    /**
     * Set up the files to be imported
     */
    private void prepareFiles() {
        if (!csvDirectoryPath.endsWith(File.separator)) {
            csvDirectoryPath += File.separator;
        }

        productFile = findFile(csvDirectoryPath, "products.csv");
        promotionsFile = findFile(csvDirectoryPath, "promotions.csv");
        categoryFile = findFile(csvDirectoryPath, "categories.csv");
        spacemanFile = findFile(csvDirectoryPath, "spaceman.csv");
        tarentSpacemanFile = findFile(csvDirectoryPath, "shelves_addon.csv");
    }

    private File findFile(String directory, final String name) {
        final File dir = new File(directory);
        final String[] list = dir.list(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.toUpperCase().equals(name.toUpperCase());
            }
        });
        if (list.length > 1) {
            LOG.warn("mulltiple files found for " + name + ". using first one.");
        } else if (list.length == 0) {
            LOG.warn("no file found for " + name + ". Not importing.");
            return null;
        }

        LOG.info("Found file: " + directory + " : " + list[0]);
        return new File(dir, list[0]);

    }

    private void cleanTables() {
        // HQL "delete from ..." does not seem to work properly since there is a primary key violation

        // so we clear the entity cache first
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();


        // and delete all data direclty with SQL
        sessionFactory.getCurrentSession().doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                PreparedStatement ps;
                ps = connection.prepareStatement("delete from category");
                ps.execute();

                ps = connection.prepareStatement("delete from product_eans");
                ps.execute();

                ps = connection.prepareStatement("delete from productlocation");
                ps.execute();

                ps = connection.prepareStatement("delete from shelf_maxxs");
                ps.execute();

                ps = connection.prepareStatement("delete from shelf");
                ps.execute();

                ps = connection.prepareStatement("delete from shelfitem");
                ps.execute();

                ps = connection.prepareStatement("delete from product");
                ps.execute();

                ps = connection.prepareStatement("delete from promotion");
                ps.execute();

            }
        });

    }


    /**
     * This is needed for testing
     *
     * @param csvDirectoryPath import directory
     */
    public void setPath(String csvDirectoryPath) {
        this.csvDirectoryPath = csvDirectoryPath;
    }

}