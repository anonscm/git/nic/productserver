package de.tarent.sellfio.csvimport;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.regex.Pattern;

/**
 * Created by mley on 02.10.14.
 */
public abstract class AbstractImport {

    private static final Logger LOG = Logger.getLogger(AbstractImport.class);

    /**
     * Process a single, split line from the current file
     * @param line String array
     * @throws ParseException if something could not be parsed.
     */
    protected abstract void processLine(String[] line) throws ParseException;

    /**
     * Starts the import
     *
     * @param delimiter field delimiter
     * @param csvFile   file to import
     */
    public void doImport(String delimiter, File csvFile) {
        if(csvFile == null) {
            return;
        }

        startFile(csvFile);

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"))){
            String line;
            while ((line = reader.readLine()) != null) {
                processLineInt(line, delimiter, csvFile);
            }
        } catch (IOException|ParseException e) {
            throw new ImportException("Error importing csv files", e);
        }
    }

    /**
     * Pre-process the line read from file.
     * @param line current line
     * @param delimiter delimiter
     * @param csvFile csvFile
     * @throws ParseException
     */
    private void processLineInt(String line, String delimiter, File csvFile) throws ParseException {
        if(line.contains("�")){
            LOG.error("Illegal encoding in input file found: "+ csvFile.getAbsolutePath()+ " : "+line);
        }
        if(line.startsWith("#")) {
            LOG.debug("Comment/Head line: "+line);
        } else {
            processLine(line.split(Pattern.quote(delimiter), -1));
        }
    }

    /**
     * Called before the file is read
     * @param file the file to be read.
     */
    public void startFile(File file) {
        // default implementation empty
    }

    /**
     * finalize the import
     */
    public void finalizeImport() {
        // default implementation empty
    }

}
