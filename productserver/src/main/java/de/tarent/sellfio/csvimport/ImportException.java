package de.tarent.sellfio.csvimport;

/**
 * Created by mley on 02.10.14.
 */
public class ImportException extends RuntimeException {

    /**
     * Creates a new Excpetion
     *
     * @param m message
     * @param t cause
     */
    public ImportException(String m, Throwable t) {
        super(m, t);
    }
}
