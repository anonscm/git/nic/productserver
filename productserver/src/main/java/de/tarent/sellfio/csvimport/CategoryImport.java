package de.tarent.sellfio.csvimport;

import de.tarent.sellfio.entities.Category;
import de.tarent.sellfio.persistence.CategoryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by mley on 02.10.14.
 */
@Component
public class CategoryImport extends AbstractImport {

    @Autowired
    CategoryDAO categoryDAO;

    /**
     * number of products per category
     */
    private Map<String, Integer> productCount;

    private List<Category> categories = new ArrayList<>();

    @Override
    protected void processLine(String[] line) {
        if (!line[0].equals("CODE") && line[0].matches("[0-9]+")) {
            final Category parsedCategory = new Category();
            parsedCategory.setCategoryId(line[0]);
            parsedCategory.setDescription(line[1]);

            parsedCategory.setNumberOfProducts(getProductCount(parsedCategory.getCategoryId()));

            categories.add(parsedCategory);
            categoryDAO.addCategory(parsedCategory);
        }
    }

    private int getProductCount(String categoryId) {
        int count = 0;
        for (String c : productCount.keySet()) {
            if (c.startsWith(categoryId)) {
                count += productCount.get(c);
            }
        }
        return count;
    }

    /**
     * Set the product quantity
     *
     * @param productCount Map of product counts
     */
    public void setProductCount(Map<String, Integer> productCount) {
        this.productCount = productCount;
    }


    @Override
    public void finalizeImport() {
        productCount.clear();
    }

    /**
     * Returns the imported categories
     *
     * @return Collection of Category objects
     */
    public Collection<Category> getCategories() {
        return categories;

    }
}
