package de.tarent.sellfio.csvimport;

import de.tarent.sellfio.entities.Product;
import lombok.Getter;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by mley on 02.10.14.
 */
@Component
public class ProductImport extends AbstractImport {

    private static final Logger LOG = Logger.getLogger(ProductImport.class);

    @Getter
    private Map<String, Integer> productCount = new HashMap<>();
    @Getter
    private Map<Integer, Product> products = new HashMap<>();

    @Override
    protected void processLine(String[] line) {

        //Prod. ID|Prod. Name|EAN-Nummer|Preis|Mengeneinheit|Irgendwas|Dies und Das|Kategorie|Kategorie ID



        try{
            final Product parsedProduct = new Product();
            parsedProduct.setArticleId(Integer.parseInt(line[0]));
            parsedProduct.setDescription(line[1]);
            parsedProduct.setEans(new HashSet<>(Arrays.asList(line[2].split(";"))));
            final String price = line[3].replaceAll("\\D", ""); // remove all non-digits
            parsedProduct.setPrice(Integer.parseInt(price));
            parsedProduct.setUnitMeasure(line[4]);
            parsedProduct.setMercId(Integer.parseInt(line[5]));
            parsedProduct.setWeighable(Integer.parseInt(line[6]) == 1);
            parsedProduct.setCategoryId(line[7]);
            countProduct(parsedProduct);
            products.put(parsedProduct.getArticleId(), parsedProduct);
        } catch (NumberFormatException|ArrayIndexOutOfBoundsException e) {
            LOG.error("Product line: (" + Arrays.toString(line) +
                    ") is missing a column or has a malformed numerical value");
        }
    }

    @Override
    public void finalizeImport() {
        productCount.clear();
    }

    private void countProduct(Product p) {
        int count = 0;
        if (productCount.containsKey(p.getCategoryId())) {
            count = productCount.get(p.getCategoryId());
        }
        count++;
        productCount.put(p.getCategoryId(), count);
    }


}
