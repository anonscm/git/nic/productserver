package de.tarent.sellfio.csvimport;

import de.tarent.sellfio.persistence.PromotionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by mley on 02.10.14.
 */
@Component
public class PromotionImport extends AbstractImport {

    @Autowired
    PromotionDAO promotionDAO;

    /**
     * italian date format
     */
    private final DateFormat dateParser = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * jdbc connection
     */
    private Connection connection;

    /**
     * prepared statement to insert data
     */
    private PreparedStatement ps;

    /**
     * id counter
     */
    private int id = 0;

    /**
     * Called before the file is read
     *
     * @param file the file to be read.
     */
    @Override
    public void startFile(File file) {
        try {
            // first remove all old data
            ps = connection.prepareStatement("delete from Promotion");
            ps.execute();

            // prepare insert statement
            ps = connection.prepareStatement("insert into Promotion " +
                    "(id, shop, promotionId, validFrom, validTo, typeId, fidelityCardRequired, articleId, " +
                    "param1, param2, param3, employeesOnly, promotionLimit, highlight ) " +
                    "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        } catch (SQLException e) {
            throw new ImportException("fail", e);
        }
    }


    @Override
    protected void processLine(String[] line) throws ParseException {
        try {
            fillData(line);
            ps.execute();
        } catch (SQLException e) {
            throw new ImportException("fail", e);
        }
    }

    // shopId;promotionId
    private void fillData(String[] line) throws SQLException, ParseException {
        //#shop|promotionId|validFrom|validTo|typeId|fidelityCardRequired|articleId|param1|param2|param3|employeesOnly|promotionLimit|highlight
        ps.setInt(1, id++); //id
        ps.setLong(2, Long.parseLong(line[0])); //shop
        ps.setLong(3, Long.parseLong(line[1])); // promotionId
        ps.setDate(4, new java.sql.Date(dateParser.parse(line[2]).getTime())); // validFrom
        // promotiion stops that day at 23:59:59.999
        ps.setDate(5, new java.sql.Date(dateParser.parse(line[3]).getTime() + 1000 * 60 * 60 * 24 - 1)); //validTo
        ps.setString(6, line[4]); //typeId
        ps.setBoolean(7, "1".equals(line[5])); //fidelityCardRequired
        ps.setInt(8, line[6].isEmpty() ? -1 : Integer.parseInt(line[6])); //articleId
        ps.setString(9, line[7]); // param1
        ps.setString(10, line[8]); // param2 (abs.discount, percent discount, special price)
        ps.setString(11, line[9]); //param3
        ps.setBoolean(12, "1".equals(line[10])); //employeesOnly
        ps.setInt(13, Integer.parseInt(line[11])); //promotionLimit
        ps.setInt(14, Integer.parseInt(line[12])); //highlight
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }


}
