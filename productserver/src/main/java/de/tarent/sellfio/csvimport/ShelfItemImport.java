package de.tarent.sellfio.csvimport;

import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.entities.ProductLocation;
import de.tarent.sellfio.entities.Shelf;
import de.tarent.sellfio.persistence.ProductDAO;
import de.tarent.sellfio.persistence.ShelfDAO;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by mley on 02.10.14.
 */
@Component
public class ShelfItemImport extends AbstractImport {

    private static final Logger LOG = Logger.getLogger(ShelfItemImport.class);

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private ShelfDAO shelfDao;

    @Setter
    private Map<Integer, Product> products;

    private String shelfName = null;
    private String currentBoard = null;

    private int currentX = 0;
    private int currentY = 0;

    private List<Shelf> shelves = new ArrayList<>();
    private List<Integer> maxXs = new ArrayList<>();

    @Override
    public void startFile(File file) {
        shelves.clear();
        maxXs.clear();
        shelfName = null;
        currentBoard = null;
        currentX = 0;
        currentY = 0;
    }

    @Override
    protected void processLine(String[] line) {
        if (line.length < 4) {
            LOG.warn("illegal line: " + Arrays.toString(line));
        }

        if (line[0].equals("Nome File")) {
            return;
        }

        checkShelfName(line[0]);
        checkBoardName(line[1]);

        final int articleId = Integer.parseInt(line[2]);
        final Product p = products.get(articleId);
        if (p != null) {
            p.getLocations().add(new ProductLocation(shelfName, currentX, currentY));
        } else {
            LOG.warn("No product found for article id " + articleId + " " + line[3]);
        }
    }

    private void checkBoardName(String currentBoardName) {
        if (currentBoardName.equals(currentBoard)) {
            currentX++;
        } else {
            if (currentBoard != null) {
                maxXs.add(currentX);
                currentY++;
            }
            currentX = 0;
            currentBoard = currentBoardName;
        }
    }

    private void checkShelfName(String currentShelfName) {
        if (!currentShelfName.equals(shelfName)) {
            if (shelfName != null) {
                maxXs.add(currentX);
                saveShelf();

                maxXs.clear();
                currentBoard = null;
                currentY = 0;
            }
            shelfName = currentShelfName;
        }
    }

    private void saveShelf() {
        final Shelf s = new Shelf();
        LOG.info("Added shelf: " + shelfName + " : " + shelfName);
        s.setShelfName(shelfName);
        s.setMaxXs(new ArrayList<>(maxXs));
        Collections.reverse(s.getMaxXs());
        shelves.add(s);
    }


    @Override
    public void finalizeImport() {
        maxXs.add(currentX);

        saveShelf();

        for (Product p : products.values()) {
            if (p.getEans().isEmpty()) {
                LOG.warn("Product without EAN: " + p.getArticleId() + " " + p.getDescription());
            }
            productDAO.addProduct(p);
        }

        for (Shelf s : shelves) {
            shelfDao.addShelf(s);
        }
    }

}
