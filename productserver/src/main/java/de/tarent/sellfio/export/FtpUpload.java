package de.tarent.sellfio.export;

import de.tarent.sellfio.exception.CheckoutException;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by mley on 11.11.14.
 */
@Service
public class FtpUpload {

    private static final Logger LOG = Logger.getLogger(FtpUpload.class);

    @Value("${export.checkdelay}")
    private int checkDelay;


    @Value("${export.ftp1.host}")
    private String host1;

    @Value("${export.ftp1.port}")
    private int port1;

    @Value("${export.ftp1.user}")
    private String user1;

    @Value("${export.ftp1.password}")
    private String password1;

    @Value("${export.ftp1.dir}")
    private String dir1;


    @Value("${export.ftp2.host}")
    private String host2;

    @Value("${export.ftp2.port}")
    private int port2;

    @Value("${export.ftp2.user}")
    private String user2;

    @Value("${export.ftp2.password}")
    private String password2;

    @Value("${export.ftp2.dir}")
    private String dir2;

    /**
     * Simple file filter class
     */
    static class Filter implements FTPFileFilter {

        private final String name;

        /**
         * Creates a new File Filter
         *
         * @param file filename to filter for
         */
        public Filter(File file) {
            this.name = file.getName();
        }

        @Override
        public boolean accept(FTPFile file) {
            return file.getName().contains(name);
        }
    }

    /**
     * Uploads a file to the configured FTP server
     *
     * @param file    file to be uploaded
     * @param timeout timeout after which watching for the uploaded file is given up in milliseconds
     * @throws CheckoutException if an error occurs
     */
    public void upload(final File file, int timeout) throws CheckoutException {
        boolean upload1Failed = false;
        try {
            upload(host1, port1, dir1, user1, password1, file, timeout);
        } catch(IOException|CheckoutException e) {
            LOG.error("Error uploading to cash-register 1", e);
            upload1Failed = true;
        }

        if(upload1Failed) {
            LOG.warn("Trying cash-register 2");
            try {
                upload(host2, port2, dir2, user2, password2, file, timeout);
            } catch (IOException e) {
                LOG.error("Error uploading to cash-register 2", e);
                throw new CheckoutException(300, "Error uploading file"); //NOSONAR stacktrace is logged
            }
        }

    }

    private void upload(String host, int port, String dir, String user, String password, File file, int timeout)
            throws CheckoutException, IOException {
        final FTPClient ftp = new FTPClient();
        try {
            connectAndLogin(ftp, host, port, user, password, dir);

            uploadFile(ftp, file);
            waitForFileRemoved(ftp, file, timeout);
        } finally {
            ftp.logout();
            ftp.disconnect();
        }
    }


    private void waitForFileRemoved(FTPClient ftp, File file, int timeout) throws IOException, CheckoutException {
        // if timeout is negative, we don't wait and always succeed.
        if(timeout < 0) {
            return;
        }

        final FTPFileFilter filter = new Filter(file);
        final long start = System.currentTimeMillis();

        while (!isTimeoutReached(start, timeout)) {
            final FTPFile[] files = ftp.listFiles(".", filter);
            LOG.info("Waiting for file to be removed. Found files: " + Arrays.toString(files));

            if (files.length == 0) {
                LOG.info("uploaded file " + file.getName() + " was removed. Upload successful.");
                return;
            } else {
                if(errorFileExists(files)) {
                    throw new CheckoutException(204, "Cash register created error file");
                }
            }
            try {
                Thread.sleep(checkDelay);
            } catch (InterruptedException e) { //NOSONAR can be ignored
            }
        }

        throw new CheckoutException(205, "Uploaded file was not removed with "+timeout);
    }

    private boolean errorFileExists(FTPFile[] files) {
        for(FTPFile f : files) {
            if(f.getName().endsWith(".err")) {
                LOG.error("Cash registers created error file: "+f.getName());
                return true;
            }
        }

        return false;
    }

    private boolean isTimeoutReached(long start, int timeout) {
        // don't wait at all
        if (timeout < 0) {
            return true;
        }

        // wait forever
        if (timeout == 0) {
            return false;
        }

        return (System.currentTimeMillis() - start) > timeout;
    }

    private void uploadFile(FTPClient ftp, File file) throws IOException, CheckoutException {
        LOG.info("Uploading file " + file.getAbsolutePath());
        final BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
        //TODO upload file with different name and rename after upload for more transaction safety
        if (!ftp.storeFile(file.getName(), bis)) {
            LOG.error("Upload failed");
            throw new CheckoutException(203, "Could not upload file");
        }

    }

    private void connectAndLogin(FTPClient ftp, String host, int port, String user, String password, String dir)
            throws IOException, CheckoutException {
        LOG.info("Connecting " + host + " " + port);
        ftp.connect(host, port);

        LOG.info("Logging in...");
        if (!ftp.login(user, password)) {
            LOG.error("FTP login failed.");
            throw new CheckoutException(201, "Could not login to FTP server");

        }

        ftp.setFileType(FTP.BINARY_FILE_TYPE);
        ftp.enterLocalPassiveMode();

        LOG.info("Changing to " + dir);
        if (!ftp.changeWorkingDirectory(dir)) {
            LOG.error("Could not change to upload directory");
            throw new CheckoutException(202, "Could not change to upload directory");
        }
    }

}
