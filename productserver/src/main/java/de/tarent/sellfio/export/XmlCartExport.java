package de.tarent.sellfio.export;

import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Cart;
import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.persistence.ProductDAO;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mley on 23.10.14.
 */
public class XmlCartExport {

    private static final Logger LOG = Logger.getLogger(XmlCartExport.class);

    private static final String SUFFIX_FILENAME = ".xml";
    private static final String PREFIX_TOTAL_EAN = "2267";
    private static final String PREFIX_FILENAME = "wag";
    private static final String PREFIX_BELEGNR = "99";

    private final DateFormat receiptDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private final DateFormat dayInYearFormatter = new SimpleDateFormat("DDD");
    private final NumberFormat articleNrFormatter = new DecimalFormat("0000000");
    private final NumberFormat totalpriceFormatter = new DecimalFormat("00000");
    private final NumberFormat belegnrFormatter = new DecimalFormat("0000");


    private ProductDAO productDAO;

    private Document doc;

    private long sumWeight = 0;
    private int sumItemCount = 0;
    private int sumPrice = 0;
    private String totalEan;
    private File outputFile;

    /**
     * Creates a new XML Cart export.
     *
     * @param productDAO ProductDAO instance
     */
    public XmlCartExport(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    /**
     * Exports the shopping cart as XML to a directory.
     *
     * @param cart      Cart object
     * @param outputDir output directory
     * @param bonDate   date of bon creation
     * @param belegNr   beleg nummer
     * @return EAN128 code
     * @throws TransformerException         if XML creation fails
     * @throws ParserConfigurationException if XML creation fails
     * @throws FileNotFoundException        if output directory does not exist
     * @throws UnsupportedEncodingException if encoding is not supported.
     */
    public String export(Cart cart, String outputDir, Date bonDate, int belegNr) throws TransformerException,
            ParserConfigurationException, FileNotFoundException, UnsupportedEncodingException {

        final Element rootElement = createDocumentAndRootElement();

        final Element tafs = doc.createElement("tafs");
        rootElement.appendChild(tafs);

        final Element articleTaf = doc.createElement("taf");
        articleTaf.setAttribute("mode", "read");
        tafs.appendChild(articleTaf);

        addMetaData(articleTaf, bonDate, belegNr);

        for (Article article : cart.getArticles()) {
            addArticle(articleTaf, article);
        }

        final Element totalTaf = doc.createElement("taf");
        totalTaf.setAttribute("mode", "read");
        tafs.appendChild(totalTaf);

        totalEan = getTotalEAN(bonDate, belegNr);

        addTotal(totalTaf, cart.getArticles().size(), totalEan);

        saveXml(outputDir, bonDate, belegNr);

        return totalEan;
    }

    /**
     * Returns the export file.
     * @return File object.
     */
    public File getOutputFile() {
        return outputFile;
    }

    private Element createDocumentAndRootElement() throws ParserConfigurationException {
        final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        doc = docBuilder.newDocument();
        doc.setXmlStandalone(true);
        final Element rootElement = doc.createElement("scaledata");
        doc.appendChild(rootElement);

        return rootElement;
    }

    private void saveXml(String outputDir, Date bonDate, int belegNr) throws TransformerException,
            UnsupportedEncodingException, FileNotFoundException {
        // write the content into xml file
        final TransformerFactory transformerFactory = TransformerFactory.newInstance();
        final Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
        transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-16");

        final DOMSource source = new DOMSource(doc);

        final String fileName = PREFIX_FILENAME + PREFIX_BELEGNR + belegnrFormatter.format(belegNr) +
                dayInYearFormatter.format(bonDate) + SUFFIX_FILENAME;
        outputFile = new File(outputDir, fileName);
        final OutputStream outputStream = new FileOutputStream(outputFile);
        final Writer fileWriter = new OutputStreamWriter(outputStream, "UTF-16");
        // Output to console for testing
        final StreamResult result = new StreamResult(fileWriter);

        transformer.transform(source, result);
    }


    private void addMetaData(Element articleTaf, Date bonDate, int belegNr) {
        addElement(articleTaf, "device_no", "1");
        addElement(articleTaf, "receipt_no", PREFIX_BELEGNR + belegnrFormatter.format(belegNr));
        addElement(articleTaf, "receipt_date", receiptDateFormatter.format(bonDate));
        addElement(articleTaf, "void_state", "0");
        addElement(articleTaf, "sales_type", "0");
    }

    private void addTotal(Element totalTaf, int numOfPositions, String totalEan) {
        final Element tafTotal = doc.createElement("taf_total");
        totalTaf.appendChild(tafTotal);

        addElement(tafTotal, "weight", Long.toString(sumWeight));
        addElement(tafTotal, "decimal_places_weight", sumWeight == 0 ? "0" : "3");
        addElement(tafTotal, "weighunit_type", sumWeight == 0 ? "0" : "1");
        addElement(tafTotal, "grossweight_control", "0");
        addElement(tafTotal, "rounding_amount", "0");
        addElement(tafTotal, "amount", Integer.toString(sumPrice));
        addElement(tafTotal, "price_to_pay", Integer.toString(sumPrice));
        addElement(tafTotal, "number_of_items", Integer.toString(sumItemCount));
        addElement(tafTotal, "number_of_positions", Integer.toString(numOfPositions));
        addElement(tafTotal, "total_ean", totalEan);
    }

    private void addArticle(Element articleTaf, Article article) {
        final Element tafRegistration = doc.createElement("taf_registration");
        articleTaf.appendChild(tafRegistration);

        final Product product = getProduct(article);

        final int total = getTotal(article, product);

        addRegistrationElements(tafRegistration, article, product, total);

        sumItemCount += article.getQuantity();
        if (product.isWeighable()) {
            sumWeight += article.getWeight();
        }
        sumPrice += total;
    }

    private void addRegistrationElements(Element tafRegistration, Article article, Product product, int total) {
        addElement(tafRegistration, "linevoid_state", "0");
        addElement(tafRegistration, "weight_origin_type", product.isWeighable() ? "0" : "1");
        addElement(tafRegistration, "price_origin_type", product.isWeighable() ? "0" : "2");
        addElement(tafRegistration, "plu_type", product.isWeighable() ? "0" : "7");
        addElement(tafRegistration, "customer_article_number", articleNrFormatter.format(product.getArticleId()));

        String ean = article.getEan();
        if(product.isEmbeddedPrice()) {
            ean = ProductDAO.getLookUpEANFromPriceEAN(ean);
        }
        addElement(tafRegistration, "customer_article_number_2", ean);

        addElement(tafRegistration, "weight", product.isWeighable() ? Integer.toString(article.getWeight()) : "0");
        addElement(tafRegistration, "decimal_places_weight", product.isWeighable() ? "3" : "0");
        addElement(tafRegistration, "weightunit_type", product.isWeighable() ? "1" : "0");
        addElement(tafRegistration, "number_of_items", Integer.toString(article.getQuantity()));
        addElement(tafRegistration, "item_baseprice", Integer.toString(product.getPrice()));
        addElement(tafRegistration, "item_originprice", Integer.toString(product.getPrice()));
        addElement(tafRegistration, "amount", Integer.toString(total));
        addElement(tafRegistration, "price_to_pay", Integer.toString(total));
    }

    private Product getProduct(Article article) {
        final Product product = productDAO.getProduct(article.getEan());

        if (product == null) {
            throw new IllegalArgumentException("could not find product for " + article.getEan());
        }

        if (article.getEan().length() == 8) {
            // if it's an EAN8, it is the articleId, but we need an EAN here.
            article.setEan(product.getEans().iterator().next());
        }

        if (product.isWeighable()) {
            article.setQuantity(1);
        }

        return product;
    }

    private int getTotal(Article article, Product product) {
        int total = 0;

        if (product.isWeighable()) {
            total = article.getWeight() * product.getPrice() / 1000;
        } else {
            total = article.getQuantity() * product.getPrice();
        }

        return total;
    }

    private void addElement(Element articleTaf, String name, String v) {
        final Element e = doc.createElement(name);
        e.appendChild(doc.createTextNode(v));
        articleTaf.appendChild(e);
    }

    private String getTotalEAN(Date bonDate, int belegNr) {
        return PREFIX_TOTAL_EAN + PREFIX_BELEGNR + belegnrFormatter.format(belegNr) +
                dayInYearFormatter.format(bonDate) + "999999" +
                totalpriceFormatter.format(sumPrice);
    }

    /**
     * Logs some statistics data
     */
    public void logSuccess() {
        LOG.info("[STATS] { " +
                "'items':" + sumItemCount + ", " +
                "'total':" + totalpriceFormatter.format(sumPrice) + ", " +
                "'totalEAN':'" + totalEan + "', " +
                "'}");
    }
}
