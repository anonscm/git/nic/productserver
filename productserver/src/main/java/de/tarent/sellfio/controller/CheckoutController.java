package de.tarent.sellfio.controller;

import de.tarent.sellfio.components.BelegNummer;
import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Cart;
import de.tarent.sellfio.entities.CheckoutResponse;
import de.tarent.sellfio.exception.CheckoutException;
import de.tarent.sellfio.export.FtpUpload;
import de.tarent.sellfio.export.XmlCartExport;
import de.tarent.sellfio.persistence.ProductDAO;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.Date;

/**
 * Created by mley on 29.10.14.
 */
@RestController
@RequestMapping("/checkout")
public class CheckoutController {

    private static final Logger LOG = Logger.getLogger(CheckoutController.class);

    private String exportDir;

    @Value("${export.uploadenabled}")
    @Setter // for testing
    private boolean uploadEnabled;

    @Autowired
    private BelegNummer belegNummer;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    @Setter // for testing
    private FtpUpload ftpUpload;

    /**
     * Creates a new CheckoutController.
     */
    public CheckoutController() {
        exportDir = getExportDir();
    }

    /**
     * Converts the given shopping cart to an XML file, that is imported into the cash register
     *
     * @param cart Shopping cart
     * @return CheckoutResponse object with an EAN code, with which the cash register can find the shopping cart data.
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Transactional(readOnly = false)
    public CheckoutResponse doCheckout(@RequestBody Cart cart) {
        LOG.info("Checkout requested: " + cart);

        try {
            checkCart(cart);

            final XmlCartExport xce = new XmlCartExport(productDAO);
            final String ean = xce.export(cart, exportDir, new Date(), belegNummer.getNextBelegNr());
            final File outputFile = xce.getOutputFile();

            LOG.info("Exported cart. Bon EAN: " + ean);

            doUpload(outputFile);

            xce.logSuccess();

            return CheckoutResponse.respond(ean);

        } catch (CheckoutException e) {
            return e.toResponse();
        } catch (Exception e) { //NOSONAR gotta catch 'em all
            LOG.error("error exporting cart to XML", e);
            return CheckoutResponse.respond(101, "Error creating XML file");
        }

    }

    private void doUpload(File outputFile) throws CheckoutException {
        if (uploadEnabled) {
            ftpUpload.upload(outputFile, 10000);
        } else {
            LOG.info("FTP upload of cart disabled.");
        }
    }

    private void checkCart(Cart cart) throws CheckoutException {
        if (cart.getArticles().isEmpty()) {
            LOG.info("Not processing empty cart");
            throw new CheckoutException(100, "Cart is empty");
        }

        for (Article a : cart.getArticles()) {
            if (a.getEan() == null || a.getEan().isEmpty()) {
                LOG.info("Empty EAN found in article. Not processing cart.");
                throw new CheckoutException(102, "cart contains article with emtpy EAN");
            }
        }
    }


    private String getExportDir() {
        final String exportDir = System.getProperty("catalina.base") + "/webapps/carts";
        final File dir = new File(exportDir);
        if (!dir.exists()) {
            dir.mkdirs(); // NOSONAR return value can be ignored
        }

        return exportDir;
    }


}
