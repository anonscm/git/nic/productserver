package de.tarent.sellfio.controller;

import de.tarent.sellfio.entities.Category;
import de.tarent.sellfio.persistence.CategoryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mley on 18.09.14.
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryDAO categoryDAO;

    /**
     * Gets the product with the given articleId
     *
     * @param categoryId category Id
     * @return Product object or null if no product with that id exists
     */
    @RequestMapping(value = "/{categoryId}", method = RequestMethod.GET)
    public Category get(@PathVariable String categoryId) {
        return categoryDAO.get(categoryId);
    }
}
