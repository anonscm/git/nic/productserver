package de.tarent.sellfio.controller;

import de.tarent.sellfio.csvimport.PennyCSVImporter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mley on 29.11.14.
 */
@RestController
@RequestMapping("/import")
public class ImportController {

    private static final Logger LOG = Logger.getLogger(ImportController.class);

    @Autowired
    private PennyCSVImporter importer;

    // TODO: the running-in-parallel-protection might be better put in the PennyCSVImporter itself because it is also
    //       triggered by a timer, without going through this ImportController.
    private volatile boolean running = false;

    /**
     * Start the import of the CSV files
     *
     * @param start flag if the shall start
     * @return current state of import
     */
    @RequestMapping(method = RequestMethod.GET, value = "/start")
    public String startImport(@RequestParam(required = false, defaultValue = "false") boolean start) {
        boolean started = false;

        if (start && !running) {
            running = true;
            new Thread() {
                @Override
                public void run() {
                    try {
                        importer.run();
                    } catch (Exception e) { //NOSONAR: we are catching all here, to have in the log file
                        LOG.error("Import Error", e);
                    }
                    running = false;
                }
            }.start();
            started = true;
        }

        return "started=" + started + " running=" + running;

    }
}
