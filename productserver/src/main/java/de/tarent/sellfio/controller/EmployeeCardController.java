package de.tarent.sellfio.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * EmployeeCardController
 */
@RestController
@RequestMapping("/card")
public class EmployeeCardController {
    private static final Logger LOG = Logger.getLogger(EmployeeCardController.class);

    /**
     * Handle login of a customer with his card id
     *
     * @param id card id
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public void handleCardLogin(@RequestParam String id) {
        LOG.info("Employee card login: " + id);
    }

    /**
     * Handle checkout of a customer with his card id
     *
     * @param id card id
     */
    @RequestMapping(value = "/checkout", method = RequestMethod.POST)
    public void handleCardCheckout(@RequestParam String id) {
        LOG.info("Employee card checkout: " + id);
    }
}
