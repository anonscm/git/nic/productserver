package de.tarent.sellfio.controller;

import de.tarent.sellfio.entities.Shelf;
import de.tarent.sellfio.persistence.ShelfDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by mley on 07.10.14.
 */
@RestController
@RequestMapping("/shelf")
public class ShelfController {

    @Autowired
    private ShelfDAO shelfDAO;

    /**
     * Returns all shelves.
     * @return List of Shelf objects
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Shelf> getAll() {
        return shelfDAO.getAll();
    }
}
