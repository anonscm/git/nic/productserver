package de.tarent.sellfio.controller;

import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Cart;
import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.entities.Promotion;
import de.tarent.sellfio.exception.InvalidCartException;
import de.tarent.sellfio.exception.ProductNotFoundException;
import de.tarent.sellfio.persistence.ProductDAO;
import de.tarent.sellfio.persistence.PromotionDAO;
import de.tarent.sellfio.promotions.PromotionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by mley on 02.10.14.
 */
@RestController
@RequestMapping("/promotion")
public class PromotionController {

    @Autowired
    private PromotionDAO promotionDAO;

    @Autowired
    private ProductDAO productDAO;

    /**
     * Get promotions for a shop.
     *
     * @param shopId shop id
     * @return List of promotions
     */
    @RequestMapping(value = "/{shopId}", method = RequestMethod.GET)
    public List<Promotion> getByShop(@PathVariable int shopId) {
        return promotionDAO.getByShop(shopId);
    }

    /**
     * Gets a cart with articles of all current valid promotions.
     * @param shopId the shop id
     * @return cart with all promoted products
     */
    @RequestMapping(value = "/{shopId}/cart", method = RequestMethod.GET)
    public Cart getPromotionCart(@PathVariable int shopId) {
        List<Promotion> promotions = promotionDAO.getByShop(shopId);

        // sort promotions by their importance
        Collections.sort(promotions, new Comparator<Promotion>() {
            @Override
            public int compare(Promotion promotion, Promotion t1) {
                return promotion.getHighlight()-t1.getHighlight();
            }
        });

        Cart cart = new Cart();

        for(Promotion p : promotions) {
            Product product = productDAO.getProductByArticleId(p.getArticleId());
            if(product != null) {
                Article a = new Article();
                a.setProduct(product);
                if (product.isWeighable()) {
                    a.setWeight(1000);
                } else {
                    a.setQuantity(1);
                }
                a.setPromotion(p);
                
                cart.getArticles().add(a);
            }
        }

        final PromotionManager pm = new PromotionManager(promotions);
        pm.applyPromotion(cart);

        return cart;
    }

    /**
     * Calculate the promotion prices for a shopping cart
     *
     * @param cart shopping cart input
     * @return shopping cart output
     */
    @RequestMapping(value = "/calculate", method = RequestMethod.POST)
    public Cart calculatePromotions(@RequestBody Cart cart) {
        final List<Promotion> promotions = promotionDAO.getByShop(952230229);

        for (Article a : cart.getArticles()) {
            if (a.getEan() == null) {
                throw new InvalidCartException("Missing EAN for " + a.toString() + ".");
            }

            final Product p = productDAO.getProduct(a.getEan());
            if (p == null) {
                throw new InvalidCartException("No product found for EAN " + a.getEan());
            }

            a.setProduct(p);
        }

        final PromotionManager pm = new PromotionManager(promotions);
        pm.applyPromotion(cart);

        return cart;

    }

}
