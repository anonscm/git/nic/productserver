package de.tarent.sellfio.controller;

import de.tarent.sellfio.persistence.CategoryDAO;
import de.tarent.sellfio.persistence.ProductDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mley on 23.09.14.
 */
@RestController
@RequestMapping("/search")
public class SearchController {

    @Autowired
    ProductDAO productDAO;

    @Autowired
    CategoryDAO categoryDAO;

    /**
     * Searches for categories and products.
     * @param search search string
     * @return Map of Lists with categories and products.
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Map<String, List> search(@RequestParam("s") String search) {
        final Map<String, List> result = new HashMap<>();

        result.put("categories", categoryDAO.search(search));
        result.put("products", productDAO.search(search));

        return result;
    }

    /**
     * Searches for products.
     * @param search search string
     * @return List of products.
     */
    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public List searchProduct(@RequestParam("s") String search) {
        return productDAO.search(search);
    }

    /**
     * Searches for products.
     * @param search search string
     * @return Limited list of products and categories.
     */
    @RequestMapping(value = "/product/top10", method = RequestMethod.GET)
    public Map<String, List> searchProductTop10(@RequestParam("s") String search) {
        final Map<String, List> result = new HashMap<>();

        result.put("categories", categoryDAO.search(search));
        result.put("products", productDAO.searchTop10(search));

        return  result;
    }
}
