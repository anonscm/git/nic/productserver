package de.tarent.sellfio.controller;

import de.tarent.sellfio.components.ProductImageResolver;
import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.exception.ProductNotFoundException;
import de.tarent.sellfio.persistence.ProductDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mley on 18.09.14.
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    private static final Logger LOG = Logger.getLogger(ProductController.class);

    @Autowired
    ProductDAO productDAO;

    @Autowired
    ProductImageResolver productImageResolver;


    /**
     * Gets all products. Not to be used in production
     *
     * @return List of Product objects
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Product> getAll() {
        return productImageResolver.fillInProductImagePaths(productDAO.getAll());
    }


    /**
     * Gets the product with the given articleId
     *
     * @param articleId article Id
     * @return Product object or null if no product with that id exists
     */
    @RequestMapping(value = "/{articleId}", method = RequestMethod.GET)
    public Product get(@PathVariable int articleId) {
        return productImageResolver.fillInProductImagePath(productDAO.getProductByArticleId(articleId));
    }

    /**
     * Gets a product by its EAN code.
     *
     * @param ean EAN code.
     * @return Product object or null if no product with that EAN code exists.
     */
    @RequestMapping(value = "/ean/{ean}", method = RequestMethod.GET)
    public Product getByEAN(@PathVariable String ean) {
        LOG.debug("Requested product for EAN "+ ean);
        Product product = null;
        try {
            product = productDAO.getProduct(ean);
            if (product == null) {
                // TODO: check if it is dangerous to echo an attackers input back to him:
                throw new ProductNotFoundException("No product for the EAN '" + ean + "' was found.");
            }
            productImageResolver.fillInProductImagePath(product);
        } catch (IllegalArgumentException e) {
            // TODO: check if it is dangerous to echo an attackers input back to him:
            throw new ProductNotFoundException("The EAN '" + ean + "' doesn't seem to be valid.");
        }
        return product;
    }


    /**
     * Gets products by their shelf
     *
     * @param shelf shelf name
     * @return Product object or null if no product with that EAN code exists.
     */
    @RequestMapping(value = "/shelf/{shelf}", method = RequestMethod.GET)
    public List<Product> getByShelf(@PathVariable String shelf) {
        return productImageResolver.fillInProductImagePaths(productDAO.getProductByShelf(shelf));
    }

    /**
     * Gets products by their category
     *
     * @param category category id
     * @return Product list or null if no product matched the category name.
     */
    @RequestMapping(value = "/category/{category}", method = RequestMethod.GET)
    public List<Product> getByCategory(@PathVariable String category) {
        if (category == null) {
            return new ArrayList<>();
        }

        return productImageResolver.fillInProductImagePaths(productDAO.getByCategory(category));
    }

}
