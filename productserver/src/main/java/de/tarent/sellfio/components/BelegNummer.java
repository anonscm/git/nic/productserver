package de.tarent.sellfio.components;

import de.tarent.sellfio.persistence.ConfigDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Calendar;

/**
 * Created by mley on 29.10.14.
 * Simple service to get beleg numbers for the dstore FTP upload. Belegnumbers start at 0 every new day. The value
 * is persisted when the application server shuts down or a new version is deployed.
 */
@Service
@Scope(value = "singleton")
public class BelegNummer {


    public static final String LAST_BELEG_NR = "LAST_BELEG_NR";
    public static final String LAST_DAY_OF_YEAR = "LAST_DAY_OF_YEAR";

    private static final Logger LOG = Logger.getLogger(BelegNummer.class);

    private volatile int belegCounter = -1;
    private volatile int lastDayOfYear = -1;

    @Autowired
    private ConfigDAO configDAO;

    /**
     * Gets a Belegnummer
     *
     * @return a new Belegnummer.
     * @throws java.lang.IllegalStateException if the object was not initialized properly.
     */
    public synchronized int getNextBelegNr() {
        if (belegCounter < 0) {
            throw new IllegalStateException("Not initialized: -1");
        }

        final Calendar calendar = Calendar.getInstance();
        final int currentDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
        // if the day changed, we have to reset the Beleg counter
        if (currentDayOfYear > lastDayOfYear) {
            LOG.info("day in year changed to " + currentDayOfYear + ". Resetting beleg counter");
            lastDayOfYear = currentDayOfYear;
            belegCounter = 0;
        }

        final int thisBelegNr = belegCounter++;

        LOG.info("Generated new belegNr: " + thisBelegNr + " day in year: " + currentDayOfYear);

        return thisBelegNr;
    }

    /**
     * Load persisted belegnr and day of year from DB.
     */
    @PostConstruct
    public synchronized void start() {
        // load beleg counter value and day from DB

                belegCounter = Integer.parseInt(configDAO.getValue(LAST_BELEG_NR, "0"));
        final String dayOfYear = Integer.toString(Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
        lastDayOfYear = Integer.parseInt(configDAO.getValue(LAST_DAY_OF_YEAR, dayOfYear));

        LOG.info("Loaded belegNr and dayOfYear: " + belegCounter + " " + lastDayOfYear);
    }

    /**
     * Persist last belegnr und day of year in DB
     */
    @PreDestroy
    public synchronized void stop() {

        configDAO.setValue(LAST_BELEG_NR, Integer.toString(belegCounter));
        configDAO.setValue(LAST_DAY_OF_YEAR, Integer.toString(lastDayOfYear));

        LOG.info("Persisted belegNr and dayOfYear: " + belegCounter + " " + lastDayOfYear);
        belegCounter = -1;
        lastDayOfYear = -1;
    }
}
