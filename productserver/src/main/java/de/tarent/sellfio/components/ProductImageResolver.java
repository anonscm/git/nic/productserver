package de.tarent.sellfio.components;

import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.util.CacheResolver;
import de.tarent.sellfio.util.SimpleCache;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.List;

/**
 * Created by mley on 20.04.15.
 */
@Component
public class ProductImageResolver {


    private SimpleCache<Integer, String> imagePathCache;

    @PostConstruct
    public void postConstruct() {
        CacheResolver<Integer, String> imagePathResolver = new CacheResolver<Integer, String>() {
            @Override
            public String resolve(Integer key) {
                String imagePath = "/pimg/"+Integer.toString(key)+".png";
                File imageFile = new File(System.getProperty("catalina.base") + "/webapps"+imagePath);
                if(imageFile.exists()) {
                    return imagePath;
                }
                return null;
            }
        };
        imagePathCache = new SimpleCache<>(imagePathResolver, 60*1000);
    }

    public List<Product> fillInProductImagePaths(List<Product> products) {
        for(Product p : products) {
            fillInProductImagePath(p);
        }
        return products;
    }

    public Product fillInProductImagePath(Product product) {
        if (product != null) {
            product.setImage(imagePathCache.get(product.getArticleId()));
        }
        return product;
    }


}
