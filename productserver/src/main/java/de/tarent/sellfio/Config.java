package de.tarent.sellfio;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * Created by mley on 26.02.15.
 */
@Configuration
@PropertySources({
        @PropertySource("classpath:version.properties"),
        @PropertySource(value = "classpath:config.properties"),
        @PropertySource(value = "file:///etc/sellfio/productserver.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "file://${CATALINA_BASE}/conf/productserver.properties",
                ignoreResourceNotFound = true)
})
public class Config {

    @Autowired
    private Environment env;

    /**
     * Enable Property source placeholders.
     * @return PropertySourcesPlaceholderConfigurer
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }


    /**
     * Get the datasource.
     * @return DataSource object
     *
     * @throws java.beans.PropertyVetoException i don't really know.
     */
    @Bean(name="dataSource")
    public DataSource dataSource() throws PropertyVetoException {
        final ComboPooledDataSource ds = new ComboPooledDataSource();
        ds.setDriverClass(env.getProperty("db.driver"));
        ds.setJdbcUrl(env.getProperty("db.url"));
        ds.setUser(env.getProperty("db.user"));
        ds.setPassword(env.getProperty("db.password"));

        ds.setAcquireIncrement(5);
        ds.setMinPoolSize(5);
        ds.setMaxPoolSize(50);
        ds.setMaxIdleTime(1800);
        ds.setIdleConnectionTestPeriod(1800);
        ds.setMaxStatements(50);

        return ds;
    }

}
