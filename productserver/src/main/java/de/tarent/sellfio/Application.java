package de.tarent.sellfio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by mley on 29.10.14.
 */
@Component
public class Application {

    @Autowired
    private AbstractApplicationContext applicationContext;

    /**
     * Register shutdown hooks.
     */
    @PostConstruct
    public void registerShutdownHook() {
        applicationContext.registerShutdownHook();
    }
}
