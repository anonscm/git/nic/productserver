package de.tarent.sellfio.persistence;

import de.tarent.sellfio.entities.ConfigItem;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mley on 29.10.14.
 */
@Repository
@Transactional
public class ConfigDAO extends AbstractDAO {

    /**
     * Gets a configuration value.
     * @param key configuration key
     * @param defaultValue default value, if no value for this key is stored yet.
     * @return persisted value or default value.
     */
    public String getValue(String key, String defaultValue) {
        final ConfigItem item = (ConfigItem)sessionFactory.getCurrentSession().get(ConfigItem.class, key);
        if(item != null) {
            return item.getValue();
        }

        return defaultValue;
    }

    /**
     * Sets a configuration value.
     * @param key key
     * @param value value
     */
    public void setValue(String key, String value) {
        ConfigItem item = (ConfigItem)sessionFactory.getCurrentSession().get(ConfigItem.class, key);
        if(item != null) {
            item.setValue(value);
            sessionFactory.getCurrentSession().save(item);
        } else {
            item = new ConfigItem();
            item.setKey(key);
            item.setValue(value);
            sessionFactory.getCurrentSession().persist(item);
        }
    }

}
