package de.tarent.sellfio.persistence;

import de.tarent.sellfio.entities.ShelfItem;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by mley on 24.09.14.
 * DAO class for ShelfItems
 */
@Repository
@Transactional
public class ShelfItemDAO extends AbstractDAO{


    /**
     * Adds a shelf item
     *
     * @param i shelf item to add
     */
    @Transactional(readOnly = false)
    public void addShelfItem(ShelfItem i) {
        sessionFactory.getCurrentSession().persist(i);
    }

    /**
     * Search ShelfItems by article id
     *
     * @param articleId article ID
     * @return List of ShelfItems with given article id
     */
    public List<ShelfItem> getShelfItemsByArticle(int articleId) {
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(ShelfItem.BY_ARTICLE);
        query.setInteger("articleId", articleId);
        return (List<ShelfItem>) query.list();
    }

   


}
