package de.tarent.sellfio.persistence;

import de.tarent.sellfio.entities.Promotion;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by mley on 02.10.14.
 */
@Repository
@Transactional
public class PromotionDAO extends AbstractDAO {

    /**
     * Add a promotion.
     * @param p Promotion to add.
     */
    public void addPromotion(Promotion p) {
        sessionFactory.getCurrentSession().persist(p);
    }

    /**
     * Get all promotions of a shop.
     * @param shopId shop identifier.
     * @return List of Promotions
     */
    public List<Promotion> getByShop(long shopId) {
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Promotion.BY_SHOP);
        query.setLong("shop", shopId);
        query.setDate("nowDate", new Date());
        return (List<Promotion>)query.list();
    }

}
