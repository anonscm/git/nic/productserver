package de.tarent.sellfio.persistence;

import de.tarent.sellfio.entities.Shelf;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by mley on 07.10.14.
 */
@Repository
@Transactional
public class ShelfDAO extends AbstractDAO {

    /**
     * Lists all shelves in the DB
     *
     * @return List of Shelf objects
     */
    public List<Shelf> getAll() {
        return (List<Shelf>) sessionFactory.getCurrentSession().getNamedQuery(Shelf.ALL).list();

    }

    /**
     * Adds a new Shelf to repository
     *
     * @param s Shelf object
     */
    @Transactional(readOnly = false)
    public void addShelf(Shelf s) {
        sessionFactory.getCurrentSession().persist(s);
    }

}
