package de.tarent.sellfio.persistence;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mley on 29.09.14.
 */
@Transactional
public abstract class AbstractDAO {

    @Autowired
    protected SessionFactory sessionFactory;

}
