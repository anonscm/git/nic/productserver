package de.tarent.sellfio.persistence;

import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.entities.ProductLocation;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by mley on 18.09.14.
 * <p/>
 * Product DAO
 */
@Repository
@Transactional
public class ProductDAO extends AbstractDAO {


    /**
     * Gets a product by its article id
     *
     * @param articleId article id
     * @return Product object or null of no product with that article id exists
     */
    public Product getProductByArticleId(int articleId) {
        return (Product) sessionFactory.getCurrentSession().get(Product.class, articleId);
    }

    /**
     * Gets all products in database
     *
     * @return List of products
     */
    public List<Product> getAll() {
        return (List<Product>) sessionFactory.getCurrentSession().getNamedQuery(Product.ALL).list();
    }


    /**
     * Gets a product by one of its EAN code.
     *
     * @param ean EAN code
     * @return Product object or null, if no product with that EAN code could be found
     */
    public Product getProductByEAN(String ean) {
        ean = cleanSearchString(ean);
        final String sql = "select * from product where _id = ( select _id from product_eans " +
                "where eans = '" + ean + "' )";
        final SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(sql);
        sqlQuery.addEntity(Product.class);

        return (Product) sqlQuery.uniqueResult();
    }

    /**
     * Search for a product with the search string in the description or shortDescription
     *
     * @param searchString search string
     * @return list of Product objects which contain the search string in their descriptions
     */
    public List<Product> search(String searchString) {

        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Product.BY_DESC);
        String search = cleanSearchString(searchString);
        search = "%" + search + "%";
        query.setString("search", search);
        return (List<Product>) query.list();
    }

    /**
     * Search for a product with the search string in the description or shortDescription
     *
     * @param searchString search string
     * @return list of Product objects which contain the search string in their descriptions
     */
    public List<Product> searchTop10(String searchString) {

        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Product.BY_DESC);
        String search = cleanSearchString(searchString);
        search = "%" + search + "%";
        query.setMaxResults(10);
        query.setString("search", search);
        return (List<Product>) query.list();
    }

    /**
     * Save a product in the Database. Caution to use this method to import lots of entites, because of session opening,
     * flushing and closing. For bulk operations we should consider another solution.
     *
     * @param p Product object to persist.
     */
    public void addProduct(Product p) {

        // ProductLocation needs the proper article ID set.
        for (ProductLocation pl : p.getLocations()) {
            pl.setArticleId(p.getArticleId());
        }

        sessionFactory.getCurrentSession().persist(p);

    }

    /**
     * Get products by category
     *
     * @param category category id
     * @return List of Products
     */
    public List<Product> getByCategory(String category) {
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Product.BY_CATEGORY);
        query.setString("category", cleanSearchString(category) + "%");
        return (List<Product>) query.list();
    }

    /**
     * Get all products in a shelf
     *
     * @param shelf shelf name
     * @return List of Products
     */
    public List<Product> getProductByShelf(String shelf) {
        shelf = cleanSearchString(shelf);
        final String sql = "select * from product  where _id in ( " +
                "select pl_articleid from productlocation where shelf = '" + shelf + "' )";
        final SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(sql);
        sqlQuery.addEntity(Product.class);

        return (List<Product>) sqlQuery.list();
    }


    /**
     * Remove invalid and potentially dangerous characters from the search string.
     *
     * @param in input
     * @return cleaned string
     */
    public static String cleanSearchString(String in) {
        if (in == null) {
            return "";
        }

        in = in.replaceAll("%", "");
        in = in.replaceAll(";", "");
        in = in.replaceAll("'", "");
        in = in.replaceAll("\"", "");

        return in;
    }

    /**
     * Updates an existing product
     *
     * @param p updated product
     */
    public void update(Product p) {
        sessionFactory.getCurrentSession().update(p);
    }


    /**
     * Gets a product by a scanned barcode. Barcode can be EAN8 or EAN13. If the EAN13 code contains the price,
     * the code is normalized and looked up. The embedded price is set in the returned product.
     *
     * @param ean ean code as string
     * @return Product object or null of no matching product is found
     * @throws java.lang.IllegalArgumentException if the EAN code is not numeric or has the wrong length
     */
    public Product getProduct(final String ean) {

        if (ean == null) {
            return null;
        }

        Product product = null;
        if (ean.length() == 13) {
            product = getProductByEAN(ean);

            if (product == null && ean.startsWith("2")) {
                // EAN is internal EAN and may have an embedded price
                final int embeddedPrice = retainPriceFromEAN(ean);
                final String lookUpEAN = getLookUpEANFromPriceEAN(ean);
                product = getProductByEAN(lookUpEAN);
                if (product != null && embeddedPrice > 0) {
                    product.setPrice(embeddedPrice);
                    product.setEmbeddedPrice(true);
                }
            }

        } else if (ean.length() == 8) {
            // barcode is article number
            try {
                final int articleNr = Integer.parseInt(ean.substring(1, 7));
                product = getProductByArticleId(articleNr);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("EAN/article nr not valid: " + ean, e);
            }
        } else {
            throw new IllegalArgumentException("EAN not valid: "+ean);
        }


        return product;
    }


    /**
     * Retain the embedded price from EAN 13. For example:
     * <p/>
     * EAN 13 = 2286290002421
     * The last 4 digits except the checksum digit are representing the price.
     * Price = 22862900|0242|1 = 2.42
     *
     * @param ean to retain the price from
     * @return the price retained from EAN 13.
     */
    protected int retainPriceFromEAN(final String ean) {
        final String possiblePriceString = ean.substring(7, 12);
        return Integer.parseInt(possiblePriceString);
    }

    /**
     * Get the lookup EAN 13 from the EAN 13 with the embedded price in it. For example:
     * <p/>
     * EAN 13 with the embedded price (2.42) = 2286290002421
     * The lookup EAN 13 will be the same EAN with zeros instead of the price digits. We also have to calculate a new
     * checksum digit after we set the price digits to zero. So the result will be:
     * Result: 2286290000007
     *
     * @param ean EAN 13 with the embedded price to generate the lookup EAN
     * @return the lookup EAN
     */
    public static String getLookUpEANFromPriceEAN(final String ean) {
        final String eanWithoutPrice = ean.substring(0, 8) + "00000";
        final String eanWithoutCheckDigit = eanWithoutPrice.substring(0, ean.length() - 1);
        return eanWithoutCheckDigit + calculateChecksumDigit(eanWithoutCheckDigit);
    }

    /**
     * Calculates the checksum digit for any EAN 13 without the check digit. For more information look here:
     * http://en.wikipedia.org/wiki/International_Article_Number_(EAN)#Calculation_of_checksum_digit
     *
     * @param eanWithoutCheckDigit EAN 13 without checksum digit (12 digits).
     * @return the new EAN with calculated checksum digit.
     */
    protected static String calculateChecksumDigit(final String eanWithoutCheckDigit) {
        int sum = 0;
        for (int i = 0; i < eanWithoutCheckDigit.length(); i++) {
            final int currentDigit = Character.getNumericValue(eanWithoutCheckDigit.charAt(i));
            if (i % 2 == 0) {
                sum = sum + currentDigit;
            } else {
                sum = sum + currentDigit * 3;
            }
        }
        return String.valueOf(10 - (sum % 10));
    }


}
