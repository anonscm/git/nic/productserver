package de.tarent.sellfio.persistence;

import de.tarent.sellfio.entities.Category;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static de.tarent.sellfio.persistence.ProductDAO.cleanSearchString;

/**
 * Created by mley on 23.09.14.
 *
 * DAO for Category
 */
@Repository
@Transactional
public class CategoryDAO extends AbstractDAO{


    /**
     * Adds a category.
     * @param c Category object
     */
    @Transactional(readOnly = false)
    public void addCategory(Category c) {
        sessionFactory.getCurrentSession().persist(c);
    }

    /**
     * Get a category.
     * @param categoryId id
     * @return Category
     */
    public Category get(String categoryId) {
        return (Category)sessionFactory.getCurrentSession().get(Category.class, categoryId);
    }


    /**
     * Searches for categories
     * @param searchString search string
     * @return List of Category objects with the searchString in their description
     */
    public List<Category> search(String searchString) {
        String search = cleanSearchString(searchString);
        search = "%"+search+"%";
        final Query query = sessionFactory.getCurrentSession().getNamedQuery(Category.BY_DESC);
        query.setString("search", search);
        return (List<Category>)query.list();
    }


}
