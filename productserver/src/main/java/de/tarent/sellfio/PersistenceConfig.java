package de.tarent.sellfio;

import org.flywaydb.core.Flyway;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.IOException;

/**
 * Created by mley on 25.11.14.
 */
@Configuration
@EnableTransactionManagement
public class PersistenceConfig {

    @Resource(name = "dataSource")
    private DataSource dataSource;


    /**
     * Start Flyway DB migration
     */
    @PostConstruct
    public void migrateDB() {
        final Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.migrate();
    }

    /**
     * Creates a Sessionfactory
     * @return SessionFactory object
     */
    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory() {

        final LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
        sessionBuilder.scanPackages("de.tarent.sellfio.entities");
        return sessionBuilder.buildSessionFactory();
    }

    /**
     * Creates a transaction manager
     * @param sessionFactory SessionFactory object
     * @return transaction manager
     */
    @Bean(name = "txName")
    public HibernateTransactionManager txName(SessionFactory sessionFactory) {
        final HibernateTransactionManager txName = new HibernateTransactionManager();
        txName.setSessionFactory(sessionFactory);
        txName.setDataSource(dataSource);
        return txName;
    }
}
