
CREATE TABLE category (
    categoryid character varying(255) NOT NULL,
    descriptionde character varying(255),
    descriptionit character varying(255),
    numberofproducts integer NOT NULL,
    PRIMARY KEY (categoryid)
);

CREATE TABLE configitem (
    key character varying(255) NOT NULL,
    value character varying(255),
    PRIMARY KEY (key)
);


CREATE TABLE product (
    _id BIGSERIAL NOT NULL,
    categoryid character varying(255),
    description character varying(255),
    embeddedprice boolean NOT NULL,
    mercid integer NOT NULL,
    price integer NOT NULL,
    shortdescription character varying(255),
    unitmeasure character varying(255),
    weighable boolean NOT NULL,
    PRIMARY KEY (_id)
);


CREATE TABLE product_eans (
    _id BIGSERIAL NOT NULL,
    ean_id serial,
    eans character varying(255),
    PRIMARY KEY (ean_id)
);



CREATE TABLE productlocation (
    _id BIGSERIAL NOT NULL,
    pl_articleid integer,
    shelf character varying(255),
    x integer NOT NULL,
    y integer NOT NULL,
    PRIMARY KEY (_id)
);


CREATE TABLE promotion (
    id BIGSERIAL NOT NULL,
    articleid integer NOT NULL,
    employeesonly boolean NOT NULL,
    fidelitycardrequired boolean NOT NULL,
    mercid integer NOT NULL,
    obligatory integer NOT NULL,
    param1 character varying(255),
    param2 character varying(255),
    param3 character varying(255),
    promotionid bigint NOT NULL,
    promotionlimit integer NOT NULL,
    shop bigint NOT NULL,
    type character varying(255),
    typeid character varying(255),
    validfrom timestamp without time zone,
    validto timestamp without time zone,
    PRIMARY KEY (id)
);


CREATE TABLE shelf (
    shelfname character varying(255) NOT NULL,
    PRIMARY KEY (shelfname)
);

CREATE TABLE shelf_maxxs (
    board_order bigint not null,
    shelf_shelfname character varying(255) NOT NULL,
    maxxs integer,
    PRIMARY KEY (shelf_shelfname, board_order)
);


CREATE TABLE shelfitem (
    shelfitemid integer NOT NULL,
    articleid integer NOT NULL,
    categoryid character varying(255),
    maxx integer NOT NULL,
    maxy integer NOT NULL,
    shelfname character varying(255),
    x integer NOT NULL,
    y integer NOT NULL,
    PRIMARY KEY (shelfitemid)
);

CREATE INDEX categoryindex ON product USING btree (categoryid);
CREATE INDEX descriptionindex ON product USING btree (description);
CREATE INDEX uk_mg2lsg987ve37del0xiu8m5rk ON shelfitem USING btree (articleid);
CREATE INDEX uk_mtx2m55aj4r23ri3p9rpsntv9 ON shelfitem USING btree (categoryid);
CREATE INDEX uk_oes2d1at2a6uwxd6hqx565hpp ON shelfitem USING btree (shelfname);

ALTER TABLE ONLY product_eans
    ADD CONSTRAINT fk_1f74ws62k2a91exoi3t2ojk55 FOREIGN KEY (_id) REFERENCES product(_id);

ALTER TABLE ONLY productlocation
    ADD CONSTRAINT fk_91oco6xk1aatddsxf1nn5bs3 FOREIGN KEY (pl_articleid) REFERENCES product(_id);

ALTER TABLE ONLY shelf_maxxs
    ADD CONSTRAINT fk_ddhyfnt6fx5psugtaj00c4n2d FOREIGN KEY (shelf_shelfname) REFERENCES shelf(shelfname);

