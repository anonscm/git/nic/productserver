package de.tarent.sellfio.components;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.persistence.ConfigDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Calendar;

import static junit.framework.Assert.assertEquals;

/**
 * Created by mley on 11.11.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class BelegNummerTest {

    @Autowired
    BelegNummer bn;

    @Autowired
    ConfigDAO configDAO;



    @Test
    public void testBelegNummer() {
        configDAO.setValue(BelegNummer.LAST_BELEG_NR, "17");
        configDAO.setValue(BelegNummer.LAST_DAY_OF_YEAR, Integer.toString(Calendar.getInstance().get(Calendar.DAY_OF_YEAR)));

        // we have to restart BelegNummer manually once more
        bn.start();

        assertEquals(17, bn.getNextBelegNr());
        assertEquals(18, bn.getNextBelegNr());
        assertEquals(19, bn.getNextBelegNr());

        bn.stop();

        assertEquals("20", configDAO.getValue(BelegNummer.LAST_BELEG_NR, "0"));

    }


    @Test
    public void testBelegNummerNewDay() {
        int dayOfYear = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        configDAO.setValue(BelegNummer.LAST_BELEG_NR, "17");
        configDAO.setValue(BelegNummer.LAST_DAY_OF_YEAR, Integer.toString(dayOfYear-1));

        // we have to restart BelegNummer manually once more
        bn.start();

        // day changed, so next new beleg nummer is 0
        assertEquals(0, bn.getNextBelegNr());
        assertEquals(1, bn.getNextBelegNr());
        assertEquals(2, bn.getNextBelegNr());

        bn.stop();

        assertEquals("3", configDAO.getValue(BelegNummer.LAST_BELEG_NR, "0"));
        assertEquals(Integer.toString(dayOfYear), configDAO.getValue(BelegNummer.LAST_DAY_OF_YEAR, "0"));

    }

    @Test(expected = IllegalStateException.class)
    public void testNotInitialized() {
        bn.stop();

        bn.getNextBelegNr();
    }

}
