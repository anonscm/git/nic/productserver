package de.tarent.sellfio.components;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.entities.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by mley on 20.04.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class ProductImageResolverTest {

    @Autowired
    ProductImageResolver productImageResolver;

    @Before
    public void setup() throws IOException {
        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        String baseDir = patternResolver.getResource("categories.csv").getFile().getParent();

        // faking catalina.base with resources dir
        // see webapps/pimg/5000.png
        System.setProperty("catalina.base", baseDir);

    }

    @Test
    public void testResolverSuccess() {
        Product p = new Product();
        p.setArticleId(5000);

        List<Product> products = new ArrayList<>();
        products.add(p);

        productImageResolver.fillInProductImagePaths(products);

        assertEquals("/pimg/5000.png", p.getImage());
    }

    @Test
    public void testResolverFail() {
        Product p = new Product();
        p.setArticleId(501);

        List<Product> products = new ArrayList<>();
        products.add(p);

        productImageResolver.fillInProductImagePaths(products);

        assertNull(p.getImage());
    }

}
