package de.tarent.sellfio.controller;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.csvimport.PennyCSVImporter;
import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Cart;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by mley on 16.04.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class PromotionControllerTest {

    @Autowired
    PromotionController promotionController;

    @Autowired
    PennyCSVImporter importer;

    @Before
    public void setup() throws IOException {
        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        String testDir = patternResolver.getResource("categories.csv").getFile().getParent() + "/short_files/";
        importer.setPath(testDir);
        importer.run();
    }

    @Test
    public void testPromotionController() {
        Cart cart = promotionController.getPromotionCart(1);

        assertNotNull(cart);

        assertEquals(4, cart.getArticles().size());

        for(Article a : cart.getArticles()) {
            assertTrue(a.isPromotionPrice());
            assertNotNull(a.getPromotion());
        }
    }
}
