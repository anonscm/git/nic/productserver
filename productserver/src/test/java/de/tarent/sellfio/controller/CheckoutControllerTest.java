package de.tarent.sellfio.controller;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Cart;
import de.tarent.sellfio.entities.CheckoutResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by mley on 02.03.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class CheckoutControllerTest {

    @Autowired
    CheckoutController cc;

    @Test
    public void testEmptyCarts() {
        Cart cart = new Cart();

        CheckoutResponse response = cc.doCheckout(cart);
        assertEquals(100, response.getStatus());

        Article a = new Article();

        a.setEan("12345678");
        cart.getArticles().add(a);

        a = new Article();
        a.setEan(null);
        cart.getArticles().add(a);

        response = cc.doCheckout(cart);
        assertEquals(102, response.getStatus());

        a.setEan("");

        response = cc.doCheckout(cart);
        assertEquals(102, response.getStatus());
    }

}
