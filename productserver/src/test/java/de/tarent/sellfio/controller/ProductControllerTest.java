package de.tarent.sellfio.controller;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.csvimport.PennyCSVImporter;
import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Cart;
import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.exception.ProductNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class ProductControllerTest {

    @Autowired
    ProductController productController;

    @Autowired
    PennyCSVImporter importer;

    @Before
    public void setup() throws IOException {
        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        String testDir = patternResolver.getResource("categories.csv").getFile().getParent() + "/short_files/";
        importer.setPath(testDir);
        importer.run();
    }

    @Test
    public void testGoodCases() {
        Product product;
        List<Product> products;

        product = productController.get(687);
        assertEquals("Club Cola", product.getDescription());

        product = productController.getByEAN("1234567894171");
        assertEquals("Reis", product.getDescription());

        products = productController.getAll();
        assertEquals(35, products.size());

        products = productController.getByCategory("6");
        assertEquals(10, products.size());

        products = productController.getByShelf("Milchprodukte");
        assertEquals(5, products.size());
    }

    // None of these should cause crashes, that's the most important point :-)
    @Test
    public void testInvalidClientInput() {
        Product product;
        List<Product> products;

        product = productController.get(9999); // This ID doesn't exist
        assertNull(product);

        product = productController.get(-1); // This ID doesn't exist
        assertNull(product);

        try {
            productController.getByEAN("1000000000000"); // This EAN doesn't exist
            fail();
        } catch (ProductNotFoundException e) {
            // ok!
        }

        try {
            productController.getByEAN("123"); // This EAN is too short
            fail();
        } catch (ProductNotFoundException e) {
            // ok!
        }

        try {
            productController.getByEAN("abcdefghijklm"); // Only digits are allowed
        } catch (ProductNotFoundException e) {
            // ok!
        }

        try {
            productController.getByEAN(null); // Only digits are allowed
        } catch (ProductNotFoundException e) {
            // ok!
        }

        products = productController.getByCategory("foo"); // This category doesn't exist
        assertTrue(products.isEmpty());

        products = productController.getByCategory(null); // Null matches no categories.
        assertTrue(products.isEmpty());

        products = productController.getByShelf("foo"); // This shelf doesn't exist
        assertTrue(products.isEmpty());

        products = productController.getByShelf(null); // This shelf doesn't exist
        assertTrue(products.isEmpty());
    }
}
