package de.tarent.sellfio.controller;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.entities.Category;
import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.persistence.CategoryDAO;
import de.tarent.sellfio.persistence.ProductDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by mley on 30.09.14.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class SearchControllerTest {

    @Autowired
    ProductDAO productDAO;

    @Autowired
    CategoryDAO categoryDAO;

    @Autowired
    SearchController searchController;

    @Test
    public void testSearchController() {

        Product p1 = new Product();
        p1.setShortDescription("Goldbären");
        p1.setDescription("Haribo Goldbären");
        p1.setUnitMeasure("PZ");
        p1.setPrice(119);
        p1.setCategoryId("131");
        p1.setArticleId(13101);
        p1.setEans(new HashSet<>(Arrays.asList(new String[]{"1234567"})));

        productDAO.addProduct(p1);

        Category c = new Category();
        c.setCategoryId("131");
        c.setDescription("Bären");

        categoryDAO.addCategory(c);

        Map<String, List> result = searchController.search("bären");
        List<Category> categories = result.get("categories");
        List<Product> products = result.get("products");

        assertEquals(p1, products.get(0));
        assertEquals(c, categories.get(0));

    }

}
