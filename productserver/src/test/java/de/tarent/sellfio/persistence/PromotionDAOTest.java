package de.tarent.sellfio.persistence;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.controller.PromotionController;
import de.tarent.sellfio.entities.Cart;
import de.tarent.sellfio.entities.Promotion;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by mley on 02.10.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class PromotionDAOTest {

    @Autowired
    PromotionDAO promotionDAO;

    @Autowired
    PromotionController promotionController;

    @Test
    public void testCalcPromotions() {
        Cart c = new Cart();
        c = promotionController.calculatePromotions(c);
        //FIXME add real test
    }

    @Test
    public void testPromotions() {
        Promotion p = new Promotion();
        p.setArticleId(1234);
        p.setFidelityCardRequired(false);
        p.setPromotionLimit(1);
        p.setShop(1234);
        p.setParam1("param1");
        p.setParam2("param2");
        p.setParam3("param3");
        Date d = new Date();
        p.setValidFrom(new Date(d.getTime() - 24*60*60*1000));
        p.setValidTo(new Date(d.getTime() + 24*60*60*1000));

        promotionDAO.addPromotion(p);

        final List<Promotion> promotions = promotionController.getByShop(1234);
        Promotion p2 = promotions.get(0);
        p2.setId(p.getId());

        assertEquals(p, p2);


    }
}
