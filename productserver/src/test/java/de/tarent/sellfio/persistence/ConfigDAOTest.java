package de.tarent.sellfio.persistence;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by mley on 29.10.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class ConfigDAOTest {

    @Autowired
    private ConfigDAO configDAO;

    @Test
    public void testConfigDAO() {
        assertEquals("bar", configDAO.getValue("foo", "bar"));

        configDAO.setValue("foo", "baz");

        assertEquals("baz", configDAO.getValue("foo", "bar"));

        configDAO.setValue("foo", "bra");

        assertEquals("bra", configDAO.getValue("foo", "bar"));

    }
}
