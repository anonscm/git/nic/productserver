package de.tarent.sellfio.persistence;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.entities.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Created by mley on 24.09.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class CategoryDAOTest {

    @Autowired
    CategoryDAO categoryDAO;


    @Test
    public void testCategoryDAO() {

        Category c = new Category();
        c.setCategoryId("666");
        c.setDescription("Belzebub");

        categoryDAO.addCategory(c);

        List<Category> result = categoryDAO.search("bub");
        assertEquals(1, result.size());
        assertEquals(c, result.get(0));


    }
}
