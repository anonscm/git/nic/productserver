package de.tarent.sellfio.persistence;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.controller.ProductController;
import de.tarent.sellfio.entities.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Created by mley on 23.09.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class ProductDAOTest {

    @Autowired
    ProductDAO productDAO;

    @Autowired
    ProductController productController;

    @Test
    public void testProductDAOAndController() {

        Product p1 = new Product();
        p1.setShortDescription("Milch");
        p1.setDescription("milfina H-Milch");
        p1.setUnitMeasure("PZ");
        p1.setPrice(119);
        p1.setCategoryId("101");
        p1.setArticleId(1001);
        p1.setEans(new HashSet<>(Arrays.asList(new String[]{"12345"})));

        productDAO.addProduct(p1);


        assertEquals(p1, productController.get(1001));
        List<Product> result = productDAO.search("h-milch");
        assertEquals(1, result.size());
        assertEquals(p1, result.get(0));

    }

    @Test
    public void testGetProduct() {
        Product p1 = new Product();
        p1.setArticleId(811010);
        p1.setEans(new HashSet<>(Arrays.asList(new String[]{"2008970000004"})));
        p1.setPrice(499);
        productDAO.addProduct(p1);

        Product p = productDAO.getProduct("2008970001998");
        assertEquals(811010, p.getArticleId());
        assertEquals(199, p.getPrice());

        p = productDAO.getProduct("28110102");
        assertEquals(811010, p.getArticleId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetProductWithInvalidEAN8() {
        productDAO.getProduct("281101aa");
    }
}
