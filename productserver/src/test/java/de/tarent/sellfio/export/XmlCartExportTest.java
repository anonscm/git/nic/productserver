package de.tarent.sellfio.export;

import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Cart;
import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.persistence.ProductDAO;
import org.junit.Test;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Date;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by mley on 23.10.14.
 */
public class XmlCartExportTest {

    @Test
    public void testExport() throws TransformerException, ParserConfigurationException, IOException {
        Product p1 = new Product();
        p1.setArticleId(1137261);
        p1.setPrice(1290);
        p1.setWeighable(true);
        p1.setUnitMeasure("KG");

        Product p2 = new Product();
        p2.setArticleId(3514);
        p2.setPrice(599);
        p2.setWeighable(false);
        p2.setUnitMeasure("PC");

        Product p3 = new Product();
        p3.setArticleId(35144);
        p3.setPrice(599);
        p3.setWeighable(false);
        p3.setUnitMeasure("PC");
        p3.setEmbeddedPrice(true);

        ProductDAO productDAO = mock(ProductDAO.class);
        when(productDAO.getProduct(eq("0000000202190"))).thenReturn(p1);
        when(productDAO.getProduct(eq("5000394017641"))).thenReturn(p2);
        when(productDAO.getProduct(eq("2286290002421"))).thenReturn(p3);

        Cart cart = new Cart();
        Article a = new Article();
        a.setEan("0000000202190");
        a.setWeight(490);
        a.setQuantity(1);
        cart.getArticles().add(a);


        a = new Article();
        a.setEan("5000394017641");
        a.setQuantity(1);
        cart.getArticles().add(a);

        a = new Article();
        a.setEan("2286290002421");
        a.setQuantity(1);
        cart.getArticles().add(a);

        // 29.10.2014 10:29:27.000
        Date exportDate = new Date(1414576767000l);

        String exportDir = System.getProperty("user.dir")+"/target/";
        File export = new File(exportDir);
        if(!export.exists()) {
            export.mkdirs();
        }
        XmlCartExport xce = new XmlCartExport(productDAO);
        xce.export(cart,exportDir, exportDate, 1);

        byte[] exported = Files.readAllBytes(new File(export, "wag990001302.xml").toPath());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        BufferedInputStream bis = new BufferedInputStream(ClassLoader.getSystemClassLoader().getResourceAsStream("wag990001302.xml"));
        int b;
        while((b=bis.read()) != -1) {
            bos.write(b);
        }
        byte[] expected = bos.toByteArray();

        assertTrue(Arrays.equals(expected, exported));


    }
}
