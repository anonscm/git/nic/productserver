package de.tarent.sellfio.export;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.exception.CheckoutException;
import org.apache.ftpserver.ConnectionConfigFactory;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.Authority;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.ConcurrentLoginPermission;
import org.apache.ftpserver.usermanager.impl.TransferRatePermission;
import org.apache.ftpserver.usermanager.impl.WritePermission;
import org.apache.ftpserver.usermanager.impl.WriteRequest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static junit.framework.Assert.assertEquals;

/**
 * Created by mley on 11.11.14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class FtpUploadTest {

    Logger LOG = LoggerFactory.getLogger(FtpUploadTest.class);

    FtpServer server;
    Set<String> files = Collections.synchronizedSet(new HashSet<String>());

    Set<String> errorize = Collections.synchronizedSet(new HashSet<String>());

    @Autowired
    FtpUpload ftpUpload;

    File testFile;

    @Before
    public void before() throws FtpException, IOException {
        File uploadDir = startFtpServer("test", "test", 2221);
        watchAndDelete(uploadDir);

        testFile = File.createTempFile("testupload", ".xml");
        BufferedWriter bw = new BufferedWriter(new FileWriter(testFile));
        bw.write("<xml>blafusel</xml>");
        bw.close();
    }

    @After
    public void after() {
        server.stop();
        testFile.delete();
    }

    @Test
    public void testUpload() throws CheckoutException {
        LOG.error("testing upload");
        ftpUpload.upload(testFile, 10000);
    }

    @Test(expected = CheckoutException.class)
    public void testUploadTimeout() throws CheckoutException {
        ftpUpload.upload(testFile, 2000);
    }

    @Test
    public void testUploadWaitForever() throws CheckoutException {
        ftpUpload.upload(testFile, 0);
    }

    @Test
    public void testUploadDontWait() throws CheckoutException {
        ftpUpload.upload(testFile, -1);
    }


    @Test(expected = CheckoutException.class)
    public void testUploadFail() throws CheckoutException {
        errorize.add(testFile.getName());
        ftpUpload.upload(testFile, 10000);
    }


    AtomicInteger failCount = new AtomicInteger(0);

    @Test
    public void testConcurrent() throws InterruptedException {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i <= 20; i++) {
            final int v = i;
            Thread t = new Thread() {
                public void run() {
                    try {
                        ftpUpload.upload(getTestFile(v), 10000);
                    } catch (Exception e) {
                        e.printStackTrace();
                        failCount.incrementAndGet();
                    }
                }
            };
            threads.add(t);
            t.start();
        }

        Thread.sleep(5000);

        // waiting for all threads to die
        for (Thread t : threads) {
            t.join();
        }

        // nothing shall have failed
        assertEquals(0, failCount.get());
    }

    @Test(expected = CheckoutException.class)
    public void testNoServer() throws CheckoutException {
        server.stop();

        //upload shall fail, because there is no server
        ftpUpload.upload(testFile, 10000);
    }

    @Test
    public void testFallback() throws CheckoutException, FtpException, IOException {
        server.stop();
        // cash1-server is stopped.
        // starting cash2 server
        File uploadDir = startFtpServer("test", "test", 2222);
        watchAndDelete(uploadDir);

        ftpUpload.upload(testFile, 10000);
    }


    public File getTestFile(int i) throws IOException {
        File f = File.createTempFile("testupload" + i, ".xml");
        BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        bw.write("<xml>blafusel</xml>");
        bw.close();

        return f;
    }

    public File startFtpServer(String username, String password, int port) throws FtpException, IOException {
        File uploadDir = File.createTempFile("FtpUploadTest", "");
        uploadDir.delete();
        uploadDir.mkdirs();

        FtpServerFactory serverFactory = new FtpServerFactory();
        ConnectionConfigFactory ccf = new ConnectionConfigFactory();
        ccf.setMaxLogins(50);
        ccf.setMaxThreads(100);
        serverFactory.setConnectionConfig(ccf.createConnectionConfig());
        UserManager um = serverFactory.getUserManager();

        BaseUser user = new BaseUser();
        user.setName(username);
        user.setPassword(password);
        user.setHomeDirectory(uploadDir.getAbsolutePath());

        List<Authority> authorities = new ArrayList<>();
        WritePermission writePerm = new WritePermission();
        writePerm.authorize(new WriteRequest(user.getHomeDirectory()));
        authorities.add(writePerm);
        authorities.add(new ConcurrentLoginPermission(0, 0));
        authorities.add(new TransferRatePermission(0, 0));
        user.setAuthorities(authorities);

        um.save(user);

        ListenerFactory factory = new ListenerFactory();

        factory.setPort(port);
        serverFactory.addListener("default", factory.createListener());
        server = serverFactory.createServer();
        server.start();

        return uploadDir;
    }

    public void watchAndDelete(final File dir) {

        new Thread() {
            public void run() {
                while (!server.isStopped() && !server.isSuspended()) {
                    File[] list = dir.listFiles();
                    for (File f : list) {
                        if (!files.contains(f.getName()) && !f.getName().endsWith(".err")) {
                            LOG.info("deleting later: " + f.getName());
                            deleteLater(f);
                        }
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

    }

    private void deleteLater(final File f) {
        files.add(f.getName());
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(errorize.contains(f.getName())) {
                    LOG.info("Errorizing " + f.getName());
                    errorize.remove(f.getName());
                    files.remove(f.getName());
                    f.renameTo(new File(f.getAbsolutePath()+".err"));

                } else {
                    LOG.info("Deleting " + f.getName());
                    files.remove(f.getName());
                    f.delete();
                }

            }
        }.start();
    }

}
