package de.tarent.sellfio.promotions;

import de.tarent.sellfio.entities.Article;
import de.tarent.sellfio.entities.Cart;
import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.entities.Promotion;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by mley on 30.10.14.
 */
public class PromotionManagerTest {

    private PromotionManager pm;

    private List<Promotion> promotions;

    private Cart cart;


    @Before
    public void setup() {
        promotions = new ArrayList<>();

        Promotion p;
        long ctm = System.currentTimeMillis();
        p = new Promotion();
        p.setTypeId(PromotionManager.PERCENT_WEIGHABLE);
        p.setArticleId(1000);
        p.setParam1("0.5");
        p.setParam2("25");
        p.setValidFrom(new Date(ctm - 86400000));
        p.setValidTo(new Date(ctm + 86400000));
        promotions.add(p);

        p = new Promotion();
        p.setTypeId(PromotionManager.PERCENT);
        p.setArticleId(1001);
        p.setParam1("0.5");
        p.setParam2("25");
        p.setValidFrom(new Date(ctm - 86400000));
        p.setValidTo(new Date(ctm+ 86400000));
        promotions.add(p);

        p = new Promotion();
        p.setTypeId(PromotionManager.ABSOLUTE);
        p.setArticleId(1002);
        p.setParam2("0.99");
        p.setValidFrom(new Date(ctm - 86400000));
        p.setValidTo(new Date(ctm + 86400000));
        promotions.add(p);


        p = new Promotion();
        p.setTypeId(PromotionManager.ABSOLUTE);
        p.setArticleId(1003);
        p.setParam2("0.99");
        p.setValidFrom(new Date(ctm - 86400000));
        p.setValidTo(new Date(ctm -1));
        promotions.add(p);

        p = new Promotion();
        p.setTypeId(PromotionManager.ABSOLUTE);
        p.setArticleId(1004);
        p.setParam2("0.99");
        p.setValidFrom(new Date(ctm + 1000*60*60*24));
        p.setValidTo(new Date(ctm + 1000*60*60*24*2));
        promotions.add(p);

        pm = new PromotionManager(promotions);

        cart = new Cart();


    }


    @Test
    public void testPromotionValid() {
        Article toTest = createArticle(1003, 199, 1, false);
        cart.getArticles().add(toTest);
        pm.applyPromotion(cart);

        assertEquals(199, cart.getArticles().get(0).getPrice());

        toTest.getProduct().setArticleId(1004);

        pm.applyPromotion(cart);

        assertEquals(199, cart.getArticles().get(0).getPrice());

    }

    @Test
    public void testPercentWeighable() {
        Article toTest = createArticle(1000, 199, 1, true);
        toTest.setWeight(600);
        cart.getArticles().add(toTest);
        pm.applyPromotion(cart);

        assertEquals(90, cart.getArticles().get(0).getPrice());

        cart.getArticles().get(0).setWeight(400);

        pm.applyPromotion(cart);
        assertEquals(80, cart.getArticles().get(0).getPrice());
    }

    @Test
    public void testPercent() {
        Article toTest = createArticle(1001, 199, 2, false);
        cart.getArticles().add(toTest);
        pm.applyPromotion(cart);

        assertEquals(298, cart.getArticles().get(0).getPrice());

    }

    @Test
    public void regressionTestLimit() throws ParseException {
        List<Promotion> promotions = new ArrayList<>();
        promotions.add(new Promotion("952230229;999994;04-SET-14;31-OTT-15;99999;SCONTO ARTICOLO €;0;790502;;;1;1;0;;2"));

        pm.setPromotions(promotions);
        addArticle("790502|tv lcd philips 201t1sb|0000000000882|149.99|PZ|92|0|9351|", 5, 0);

        pm.applyPromotion(cart);
        assertEquals(2, cart.getArticles().size());
        assertTrue(cart.getArticles().get(0).isPromotionPrice());
        assertFalse(cart.getArticles().get(1).isPromotionPrice());
    }


    @Test
    public void testAbsolute() {
        Article toTest = createArticle(1002, 199, 2, false);
        cart.getArticles().add(toTest);
        pm.applyPromotion(cart);

        assertEquals(200, cart.getArticles().get(0).getPrice());

    }

    private Article createArticle(int articleId, int price, int quantity, boolean weighable) {
        Product p = new Product();
        p.setArticleId(articleId);
        p.setPrice(price);
        p.setWeighable(weighable);

        Article a = new Article();
        a.setProduct(p);
        a.setQuantity(quantity);
        return a;
    }

    @Test
    public void testPrezzoSpeciale() throws ParseException {
        List<Promotion> promotions = new ArrayList<>();

        promotions.add(new Promotion("952230229;40530;15-SET-14;31-OTT-15;ccb4cc62-b65d-4117-82d6-e264d2d7cc2d;SCONTO PREZZO SPECIALE;0;759230;;;,001;,89;0;;0;;;;;;;;;;;"));
        pm.setPromotions(promotions);

        addArticle("759230|Insalata ricca or.Italia 180g|8003561510003;8034073870651;8024370005061;8033857783095|0.99|KG|4|0|1311|", 1, 0);

        pm.applyPromotion(cart);

        assertEquals(89, cart.getArticles().get(0).getPrice());
    }


    @Test
    public void testSimplePromotions() throws ParseException {
        List<Promotion> promotions = new ArrayList<>();

        promotions.add(new Promotion("952230229;999991;15-SET-14;31-OTT-15;84e6e5d0-ac1e-11de-8a39-0800200c9a66;SCONTO ARTICOLO % FRUTTA SFUSA;0;761090;;;0,001;20;0;;"));
        promotions.add(new Promotion("952230229;999992;15-SET-14;31-OTT-15;94e6e5d0-ac1e-11de-8a39-0800200c9a66;SCONTO ARTICOLO % NO FRUTTA SFUSA;0;759230;;;1;50;0;;"));
        promotions.add(new Promotion("952230229;999993;04-SET-14;31-OTT-15;99999;SCONTO ARTICOLO €;0;790503;;;1;10;0;;"));
        promotions.add(new Promotion("952230229;999994;04-SET-14;31-OTT-15;99999;SCONTO ARTICOLO €;0;790502;;;1;1;0;;"));

        pm.setPromotions(promotions);


        //regression test: quantity is here 0 for a weighable product, because quantity is not sent in json for weighable products
        addArticle("761090|Zucchine Bianche al kg.|0000000000734|0.89|KG|4|1|1245|", 0, 1000);


        addArticle("759230|Insalata ricca or.Italia 180g|8003561510003;8034073870651;8024370005061;8033857783095|0.99|KG|4|0|1311|", 1, 0);
        addArticle("790503|bicicletta olanda 28 uomo|0000000000883|79.99|PZ|92|0|9394|", 1, 0);
        addArticle("790502|tv lcd philips 201t1sb|0000000000882|149.99|PZ|92|0|9351|", 1, 0);

        pm.applyPromotion(cart);


        /*
        1
        761090|Zucchine Bianche al kg.|0000000000734|0.89|KG|4|1|1245|
        no promotion
        - add product 761090,  "Zucchine Bianche al kg."
        - add weight 1000g
        - product is in the shopping basket
        - the old price is striked out "0,89"
        - the new price is reduced by "20%" = "0,71"
        */
        assertEquals(71, cart.getArticles().get(0).getPrice());


        /*
        2
        759230|Insalata ricca or.Italia 180g|8003561510003;8034073870651;8024370005061;8033857783095|0.99|KG|4|0|1311|
        - add product 844690, "Insalata ricca or.Italia 180g"
        - product is in the shopping basket
        - the old price is striked out "0,99"
        - the new price is reduced by "50%" = "0,49"
        */
        assertEquals(49, cart.getArticles().get(1).getPrice());


        /*
        3
        790503|bicicletta olanda 28 uomo|0000000000883|79.99|PZ|92|0|9394|
        - add product 790503, "bicicletta olanda 28 uomo"
        - product is in the shopping basket
        - the old price is striked out "79.99"
        - the new price is reduced by "10€" = "69.99"
        */
        assertEquals(6999, cart.getArticles().get(2).getPrice());


        /*
        4
        790502|tv lcd philips 201t1sb|0000000000882|149.99|PZ|92|0|9351|
        - add product 790502, "tv lcd philips 201t1sb"
        - product is in the shopping basket
        - the old price is striked out "149.99"
        - the new price is reduced by "1€" = "148.99"
         */
        assertEquals(14899, cart.getArticles().get(3).getPrice());
    }

    private Article addArticle(String product, int quantity, int weight) {
        Product p = new Product(product);
        Article a = new Article();
        a.setProduct(p);
        a.setQuantity(quantity);
        a.setWeight(weight);

        cart.getArticles().add(a);

        return a;
    }

    @Test
    public void testPennyAndEmployeeCard() throws ParseException {
        List<Promotion> promotions = new ArrayList<>();

        promotions.add(new Promotion("952230229;999991;15-SET-14;31-OTT-30;84e6e5d0-ac1e-11de-8a39-0800200c9a66;SCONTO ARTICOLO % FRUTTA SFUSA;0;761089;;;0,001;20;0;;1;"));
        promotions.add(new Promotion("952230229;999995;15-SET-14;31-OTT-30;84e6e5d0-ac1e-11de-8a39-0800200c9a66;SCONTO ARTICOLO % FRUTTA SFUSA;1;761089;;;0,001;10;0;;1;"));
         promotions.add(new Promotion("952230229;999996;15-SET-14;31-OTT-30;84e6e5d0-ac1e-11de-8a39-0800200c9a66;SCONTO ARTICOLO % FRUTTA SFUSA;0;761089;;;0,001;20;0;1;1;"));

        pm.setPromotions(promotions);



        /*
         * 1
         - add product 761089, "Fave fresche Or.IT cat.I al kg"
         - add weight 1000g
         - product is in the shopping basket
         - the old price is striked out "1.19"
         - the new price is reduced by "20%" = "0,95"
        */
        addArticle("761089|Fave fresche Or.IT cat.I al kg|0000000000732|1.19|KG|4|1|1246|", 1, 1000);

        pm.applyPromotion(cart);

        assertEquals(95, cart.getArticles().get(0).getPrice());

        /*
         2
         - add AGAIN product 761089, "Fave fresche Or.IT cat.I al kg"
         - add weight 1000g
         - product is in the shopping basket
         - the weight is now 2000g
         - the old price is striked out "1.19"
         - the new price is reduced by "20%" = "0,95"
         => the Limit will be ignored!
         */
        addArticle("761089|Fave fresche Or.IT cat.I al kg|0000000000732|1.19|KG|4|1|1246|", 1, 1000);

        pm.applyPromotion(cart);

        assertEquals(95, cart.getArticles().get(0).getPrice());


        assertEquals(95, cart.getArticles().get(1).getPrice());





          /*
         3
         - delete shopping basket
         - scan employee card
         - add product 761089, "Fave fresche Or.IT cat.I al kg"
         - add weight 1000g
         - only prodct is in the shopping basket
         - the old price is striked out "1.19"
         - the new price is calculated (20% general promotion, 20% employee card promo)
         = 1.19* (1-0,2) *(1-0,2)
         = "0,76"
        */
        cart.getArticles().clear();
        cart.getAdditionalBarcodes().add("2090010000000");
        addArticle("761089|Fave fresche Or.IT cat.I al kg|0000000000732|1.19|KG|4|1|1246|", 1, 1000);

        pm.applyPromotion(cart);

        assertEquals(76, cart.getArticles().get(0).getPrice());



         /*
         4
         - delete shopping basket
         - scan penny card
         - add product 761089, "Fave fresche Or.IT cat.I al kg"
         - add weight 1000g
         - only prodct is in the shopping basket
         - the old price is striked out "1.19"
         - the new price is calculated (20% general promotion, 10% penny card promo)
         = 1.19* (1-0,2) *(1-0,1)
         = "0,85"
         */
        cart.getArticles().clear();
        cart.getAdditionalBarcodes().remove("2090010000000");
        cart.getAdditionalBarcodes().add("2095010000000");
        addArticle("761089|Fave fresche Or.IT cat.I al kg|0000000000732|1.19|KG|4|1|1246|", 1, 1000);

        pm.applyPromotion(cart);

        assertEquals(85, cart.getArticles().get(0).getPrice());
    }


    @Test
    public void testLimits() throws ParseException {

        List<Promotion> promotions = new ArrayList<>();
        promotions.add(new Promotion("952230229;999997;15-SET-14;31-OTT-30;94e6e5d0-ac1e-11de-8a39-0800200c9a66;SCONTO ARTICOLO % NO FRUTTA SFUSA;0;114256;;;1;5;0;;2;"));
        promotions.add(new Promotion("952230229;999998;15-SET-14;31-OTT-30;94e6e5d0-ac1e-11de-8a39-0800200c9a66;SCONTO ARTICOLO % NO FRUTTA SFUSA;1;114256;;;1;5;0;;1;"));
        promotions.add(new Promotion("952230229;999999;15-SET-14;31-OTT-30;94e6e5d0-ac1e-11de-8a39-0800200c9a66;SCONTO ARTICOLO % NO FRUTTA SFUSA;0;114256;;;1;20,33;0;1;1;"));

        pm.setPromotions(promotions);

        /**
         * 1
         - add product 114256, "Fusilli Pasta di Gragnano IGP"
         - product is in the shopping basket
         - the old price is striked out "0,75"
         - the new price is reduced by "5%" = "0,71"
         */
        addArticle("114256|Fusilli Pasta di Gragnano IGP|8010309021400|0.75|KG|1|0|3194|", 1, 0);

        pm.applyPromotion(cart);

        assertEquals(71, cart.getArticles().get(0).getPrice());


        /*
         2
         - add AGAIN product 114256, "Fusilli Pasta di Gragnano IGP" (change number items manually or scan again)
         - product is in the shopping basket
         - the number of items is now 2
         - the old price is striked out "0,75"
         - the new price is reduced by "5%" = "0,71"
         => the Limit is not yet reached (Limit = 2)
         */
        cart.getArticles().get(0).setQuantity(2);
        pm.applyPromotion(cart);
        assertEquals(142, cart.getArticles().get(0).getPrice());

        /*
         3
         - add AGAIN product 114256, "Fusilli Pasta di Gragnano IGP" (change number items manually or scan again)
         - product is in the shopping basket
         - the number of items is now 3
         => the Limit is reached (Limit = 2)
         - split in 2 entries, => the Limit is reached (Limit = 2)
         - one entry= 2 items with promo, old price is striked out "0,75", new price reduced by "5%" = "0,71"
         - one entry= 1 item without promo, one price "0,75"
         - the new sum is
         2* reduced by "5%" = "0,71"
         1* original price = "0,75"

         BUT the calculation is always the same
         2* reduced by "5%" = "0,71"
         1* original price = "0,75"
        */
        cart.getArticles().get(0).setQuantity(3);
        pm.applyPromotion(cart);

        assertEquals(142, cart.getArticles().get(0).getPrice());

        // additional article in cart with non-promoted product
        assertEquals(75, cart.getArticles().get(1).getPrice());
        assertFalse(cart.getArticles().get(1).isPromotionPrice());

        /*
         4
         - scan employee card
         - update basket based on employee promotions
         calculation is the same
         2* reduced by "5%" = "0,71"
         1* reduced by "5%", then by "20%" = "0,57" (5% general promotion, 20% employee promo)
         */

        cart.getAdditionalBarcodes().add("2090010000000");
        pm.applyPromotion(cart);

        assertEquals(56, cart.getArticles().get(0).getPrice());
        // additional article in cart with non-promoted product
        assertEquals(71, cart.getArticles().get(1).getPrice());
        assertEquals(75, cart.getArticles().get(2).getPrice());

         /*
         5
         - delete employee card
         - scan penny card
         - update basket based on penny promotions

         BUT the calculation is the same
         2* reduced by "5%" = "0,71"
         1* reduced by "5%", then by "5%" = "0,68" (5% general promotion, 5% penny card promo)
         */

        cart.getAdditionalBarcodes().remove("2090010000000");
        cart.getAdditionalBarcodes().add("2095010000000");
        pm.applyPromotion(cart);

        assertEquals(67, cart.getArticles().get(0).getPrice());
        // additional article in cart with non-promoted product
        assertEquals(71, cart.getArticles().get(1).getPrice());
        assertEquals(75, cart.getArticles().get(2).getPrice());
    }

    @Test
    public void testLimits2() throws ParseException {
        List<Promotion> promotions = new ArrayList<>();


         promotions.add(new Promotion("952230229;999994;04-SET-14;31-OTT-30;99999;SCONTO ARTICOLO €;0;790502;;;1;10.00;0;;1;"));
         promotions.add(new Promotion("952230229;999995;04-SET-14;04-OTT-30;99999;SCONTO ARTICOLO €;0;790502;;;1;24,00;0;1;1;"));
         promotions.add(new Promotion("952230229;999996;04-SET-14;04-OTT-30;99999;SCONTO ARTICOLO €;1;790502;;;1;12;0;;1"));

        pm.setPromotions(promotions);

        /*
         * 1
         - add product 790502, "tv lcd philips 201t1sb"
         - product is in the shopping basket
         - the old price is striked out "149.99"
         - the new price is reduced by "10€" = "139,99"
         */
        addArticle("790502|tv lcd philips 201t1sb|0000000000882|149.99|PZ|92|0|9351|", 1, 0);

        pm.applyPromotion(cart);
        assertEquals(13999, cart.getArticles().get(0).getPrice());

        /*
         3
         - add AGAIN product 790502, "tv lcd philips 201t1sb" (change number items manually or scan again)
         - product is in the shopping basket
         - split in 2 entries, => the Limit is reached (Limit = 1)
         - one entry= 1 item with promo, old price is striked out "149,99", new price reduced by "1€" = "139,99"
         - one entry= 1 item without promo, one price "149,99"
         1* reduced by "10€" = "139,99"
         1* original price = "149,99"

         */
        addArticle("790502|tv lcd philips 201t1sb|0000000000882|149.99|PZ|92|0|9351|", 1, 0);

        pm.applyPromotion(cart);
        assertEquals(13999, cart.getArticles().get(0).getPrice());
        assertEquals(14999, cart.getArticles().get(1).getPrice());

         /*

         4
         - scan employee card
         - update basket based on employee promotions
         1* reduced by "10€+24€" = "115,99"
         1* original price = "149,99"

         */

        cart.getAdditionalBarcodes().add("2090010000000");
        pm.applyPromotion(cart);
        assertEquals(11599, cart.getArticles().get(0).getPrice());
        assertEquals(14999, cart.getArticles().get(1).getPrice());
         /*

         5
         - delete employee card
         - scan penny card
         - update basket based on penny promotions
         1* reduced by "10€+12€" = "136,99"
         1* original price = "149,99"
         */
        cart.getAdditionalBarcodes().remove("2090010000000");
        cart.getAdditionalBarcodes().add("2095010000000");
        pm.applyPromotion(cart);
        assertEquals(12799, cart.getArticles().get(0).getPrice());
        assertEquals(14999, cart.getArticles().get(1).getPrice());
    }

    @Test
    public void regressionTestNotCompactingEmbeddedPriceProducts() {
        addArticle("761087|Fave fresche Or.IT cat.I al kg|2337990001006|1.19|KG|4|0|1246|", 1, 1000);
        cart.getArticles().get(0).getProduct().setEmbeddedPrice(true);
        addArticle("761087|Fave fresche Or.IT cat.I al kg|2337990001006|1.19|KG|4|0|1246|", 1, 1000);
        cart.getArticles().get(1).getProduct().setEmbeddedPrice(true);


        pm.applyPromotion(cart);

        assertEquals(2, cart.getArticles().size());
    }

 }
