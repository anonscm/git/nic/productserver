package de.tarent.sellfio.csvimport;

import de.tarent.sellfio.TestConfig;
import de.tarent.sellfio.TestPersistenceConfig;
import de.tarent.sellfio.entities.Category;
import de.tarent.sellfio.entities.Product;
import de.tarent.sellfio.entities.ProductLocation;
import de.tarent.sellfio.entities.Promotion;
import de.tarent.sellfio.entities.Shelf;
import de.tarent.sellfio.persistence.CategoryDAO;
import de.tarent.sellfio.persistence.ProductDAO;
import de.tarent.sellfio.persistence.PromotionDAO;
import de.tarent.sellfio.persistence.ShelfDAO;
import de.tarent.sellfio.persistence.ShelfItemDAO;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import javax.transaction.Transactional;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;

@TransactionConfiguration(transactionManager = "txName")
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class, TestPersistenceConfig.class})
public class PennyCSVImporterTest {

    @Autowired
    private PennyCSVImporter toTest;

    @Autowired
    ProductDAO productDAO;

    @Autowired
    CategoryDAO categoryDAO;

    @Autowired
    ShelfItemDAO shelfItemDAO;

    @Autowired
    PromotionDAO promotionDAO;

    @Autowired
    ShelfDAO shelfDAO;

    @Autowired
    SessionFactory sessionFactory;


    @Before
    public void setup() throws IOException {
        System.setProperty("catalina.base", "/tmp");

        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        String testDir = patternResolver.getResource("categories.csv").getFile().getParent() + "/short_files/";
        toTest.setPath(testDir);
        toTest.run();
    }

    @Test
    public void testRun() throws Exception {


        Product testProduct = productDAO.getProductByArticleId(139);
        assertTrue(testProduct.getEans().contains("1234567892627"));

        Category testCategory = categoryDAO.search("milch").get(0);
        assertTrue(testCategory.getDescription().equals("Milchprodukte"));


        testProduct = productDAO.getProductByArticleId(505);
        ProductLocation pl = testProduct.getLocations().iterator().next();
        assertEquals("Fleischwaren", pl.getShelf());
        assertEquals(1, pl.getX());
        assertEquals(0, pl.getY());


        Category fleischwaren = categoryDAO.get("3");
        assertEquals(2, fleischwaren.getNumberOfProducts());


        List<Promotion> promotions = promotionDAO.getByShop(1);
        Promotion promotion = null;
        for (Promotion p : promotions) {
            if (p.getArticleId() == 139) {
                promotion = p;
                break;
            }
        }

        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

        assertEquals("SPECIAL", promotion.getTypeId());
        assertEquals(df.parse("01.01.2015"), promotion.getValidFrom());
        assertEquals(df.parse("01.01.2099"), promotion.getValidTo());

        List<Product> products = productDAO.getProductByShelf("Getraenke");
        assertEquals(5, products.size());


        List<Shelf> shelves = shelfDAO.getAll();
        assertEquals(6, shelves.size());
        boolean foundShelf = false;
        for(Shelf s : shelves) {
            if("Getraenke".equals(s.getShelfName())) {
                foundShelf = true;
                assertEquals(4, s.getMaxXs().get(0).intValue());
            }
        }
        assertTrue(foundShelf);

    }

    @Test
    public void regressionTestProperImportEncoding() {
        // files from penny are encoded in ISO-8859-15, not UTF-8
        Product p = productDAO.getProductByArticleId(276);
        assertEquals("Frühstücks-Konfitüre Extra", p.getDescription());
    }

}
