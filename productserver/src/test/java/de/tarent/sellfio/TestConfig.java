package de.tarent.sellfio;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * Created by mley on 26.02.15.
 */
@Configuration
@PropertySources({
        @PropertySource(value = "classpath:config.properties"),
})
@ComponentScan(value = {"de.tarent.sellfio.components", "de.tarent.sellfio.controller", "de.tarent.sellfio.csvimport",
        "de.tarent.sellfio.export", "de.tarent.sellfio.persistence", "de.tarent.sellfio.promotions"})
public class TestConfig {

    /**
     * Enable Property source placeholders.
     * @return PropertySourcesPlaceholderConfigurer
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }


    /**
     * Get the datasource
     * @return DataSource object
     */
    @Bean(name="dataSource")
    public DataSource dataSource() throws PropertyVetoException {
        ComboPooledDataSource ds = new ComboPooledDataSource();
        ds.setDriverClass("org.h2.Driver");
        ds.setJdbcUrl("jdbc:h2:target/inmem-testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE");
        ds.setUser("h2");
        ds.setPassword("h2");

        ds.setAcquireIncrement(5);
        ds.setMinPoolSize(5);
        ds.setMaxPoolSize(50);
        ds.setMaxIdleTime(1800);
        ds.setIdleConnectionTestPeriod(1800);
        ds.setMaxStatements(50);

        return ds;
    }

}
