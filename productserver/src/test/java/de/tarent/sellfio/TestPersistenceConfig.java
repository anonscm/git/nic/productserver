package de.tarent.sellfio;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.IOException;

/**
 * Created by mley on 27.02.15.
 */
@Configuration
@EnableTransactionManagement
public class TestPersistenceConfig {

    @Resource(name = "dataSource")
    private DataSource dataSource;




    /**
     * Creates a Sessionfactory
     * @return SessionFactory object
     */
    @Autowired
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory() {

        final LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
        sessionBuilder.scanPackages("de.tarent.sellfio.entities");

        //TODO also use Flyway in tests
        sessionBuilder.getProperties().setProperty("hibernate.hbm2ddl.auto", "create-drop");
        sessionBuilder.getProperties().setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");

        return sessionBuilder.buildSessionFactory();
    }

    /**
     * Creates a transaction manager
     * @param sessionFactory SessionFactory object
     * @return transaction manager
     * @throws java.io.IOException if something fails
     */
    @Bean(name = "txName")
    public HibernateTransactionManager txName(SessionFactory sessionFactory) throws IOException {
        final HibernateTransactionManager txName = new HibernateTransactionManager();
        txName.setSessionFactory(sessionFactory);
        txName.setDataSource(dataSource);
        return txName;
    }

}
