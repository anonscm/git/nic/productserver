#!bin/bash

# The directory where the logfiles are stored.
DIRECTORY="/var/log/tomcat7"
#DIRECTORY="/home/dez/carts"

# The date.
DATE=""

# Check if a date was manually given. If so, use it, but check that it's in the correct format first. If no date
# was given, then use yesterday's date.
if [ "$#" -gt 0 ]
then
	DATE=$1
	$(date "+%Y-%m-%d" -d $DATE 2>1 > /dev/null)
	if [ $? -ne 0 ]
	then
		echo "The following date is in an invalid format: $DATE. Please use the following format: YYYY-MM-DD."
		exit 0
	fi
else
	DATE=$(date --date '-1 days' +"%Y-%m-%d")
fi

# Get the productserver.log.gz, copy it to /tmp, remove the productserver.log from the /tmp and unpack the
# new one.
if [ -d "$DIRECTORY" ]
then
	rm -rf /tmp/productserver.log.gz /tmp/productserver.log

	# Check if file exists.
	if [[ -f "$DIRECTORY/productserver.log.gz" && -f "$DIRECTORY/productserver.log" ]]
	then
		cp $DIRECTORY/productserver.log.gz /tmp
		cd /tmp
		gzip -d productserver.log.gz
		cat $DIRECTORY/productserver.log >> productserver.log
	elif [ -f "$DIRECTORY/productserver.log" ]
	then
		cp $DIRECTORY/productserver.log /tmp
		cd /tmp
	else
		echo "The log files $DIRECTORY/productserver.log and $DIRECTORY/productserver.log.gz do not exist."
		exit 0
	fi

	# Remove the old csv and create the new one with the headers.
	rm -rf receipt_info_$DATE.csv
	echo "time,ean,items,total,employeeCard" >> receipt_info_$DATE.csv

	# Go through each line, extract only the important values and add each as a line to the csv.
	grep $DATE productserver.log | grep INFO | grep STATS | while read -r line ;
	do
		TEMP=$(echo $line | grep -Po "'items':(\d*?,|.*?\[^\\],)")
		ITEM=$(echo $TEMP | sed 's/[^0-9]*//g')
		
		TEMP=$(echo $line | grep -Po "'total':(\d*?,|.*?\[^\\],)")
		TOTAL=$(echo $TEMP | sed 's/[^0-9]*//g')
		
		EAN=$(echo $line | grep -o "'totalEAN':'[^']*'" | cut -d "'" -f4)
		
		TIME=$(echo $line | awk '{print substr($0,0,19)}')
		
		EMPLOYEE_CARD=$(echo $line | grep -o "'cardId':'[^']*'" | cut -d "'" -f4)
		
		echo "$TIME,$EAN,$ITEM,$TOTAL,$EMPLOYEE_CARD" >> receipt_info_$DATE.csv
	done
else
	echo "Directory $DIRECTORY does not exist."
fi
